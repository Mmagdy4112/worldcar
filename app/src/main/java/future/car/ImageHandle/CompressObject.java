package future.car.ImageHandle;

import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;

/**
 * Created by mac on 4/24/17.
 */

public class CompressObject {

    private Bitmap image=null;
    private ByteArrayOutputStream byteStream=null;
    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public ByteArrayOutputStream getByteStream() {
        return byteStream;
    }

    public void setByteStream(ByteArrayOutputStream byteStream) {
        this.byteStream = byteStream;
    }
}