package future.car.Adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import future.car.Model.MetaDataModel;
import future.car.R;


/**
 * Created by Mohamed Magdy on 03/11/2017.
 */

public class TransferAdapter extends RecyclerView.Adapter<TransferAdapter.TransferHolder> {
    public List<MetaDataModel.BankAccount> bankAccounts;
    private Context context;


    public TransferAdapter(List<MetaDataModel.BankAccount> bankAccounts, Context context) {
        this.bankAccounts = bankAccounts;
        this.context = context;

    }

    @Override
    public TransferHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TransferHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transfer_banks_item, parent, false));
    }

    @Override
    public void onBindViewHolder(TransferHolder holder, int position) {
        MetaDataModel.BankAccount bankAccount = bankAccounts.get(position);

        holder.name.setText(""+bankAccount.bankNameAr);
        holder.address.setText(""+bankAccount.ownerNameAr);
        holder.accNum.setText(""+bankAccount.accountNumber);
        holder.eban.setText(""+bankAccount.ipanNumber);



    }

    public int getItemCount() {
        return bankAccounts.size();
    }

    public class TransferHolder extends RecyclerView.ViewHolder {
        AppCompatTextView name,address,accNum,eban;
        ImageView image;

        public TransferHolder(View itemView) {
            super(itemView);
            name =  itemView.findViewById(R.id.tv_transfer_item_name);
            address =  itemView.findViewById(R.id.tv_transfer_item_address);
            accNum =  itemView.findViewById(R.id.tv_transfer_item_account_number);
            eban =  itemView.findViewById(R.id.tv_transfer_item_eban);



        }

  
    }

}
