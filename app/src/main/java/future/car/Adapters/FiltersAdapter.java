package future.car.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import future.car.R;
import future.car.Utils.RecyclerViewClickListener;


/**
 * Created by Mohamed Magdy on 03/11/2017.
 */

public class FiltersAdapter extends RecyclerView.Adapter<FiltersAdapter.FiltersHolder> {
    private Context context;
    private RecyclerViewClickListener itemListener;
    String [] nameArr;


    public FiltersAdapter(String []  nameArr, Context context, RecyclerViewClickListener itemListener) {
        this.nameArr = nameArr;
        this.context = context;
        this.itemListener = itemListener;

    }

    @Override
    public FiltersHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FiltersHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.filters_item, parent, false));
    }

    @Override
    public void onBindViewHolder(FiltersHolder holder, int position) {


        holder.name.setText(nameArr[position]);




    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public int getItemCount() {
        return nameArr.length;
    }

    public class FiltersHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name;

        public FiltersHolder(View itemView) {
            super(itemView);
            name =  itemView.findViewById(R.id.tv_filters_item);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v, this.getLayoutPosition());

        }
    }

}
