package future.car.Adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import future.car.Model.CarBidModel;
import future.car.R;
import future.car.Utils.Tools;


/**
 * Created by Mohamed Magdy on 03/11/2017.
 */

public class BidHistoryAdapter extends RecyclerView.Adapter<BidHistoryAdapter.BidHistoryHolder> {
    public List<CarBidModel.Bid> carBids;
    private Context context;
    boolean carOwner = false;


    public BidHistoryAdapter(List<CarBidModel.Bid> carBids, Context context, boolean carOwner) {
        this.carBids = carBids;
        this.context = context;
        this.carOwner = carOwner;

    }

    @Override
    public BidHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BidHistoryHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bid_history_item, parent, false));
    }

    @Override
    public void onBindViewHolder(BidHistoryHolder holder, int position) {
        final CarBidModel.Bid carBid = carBids.get(position);

        holder.name.setText(""+carBid.firstName);
        holder.price.setText(""+carBid.price);
        holder.time.setText(""+carBid.createdAt);

        if (!carOwner){
            holder.call.setVisibility(View.GONE);
        }

        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.makeCall(context,carBid.mobile);
            }
        });


    }

    public int getItemCount() {
        return carBids.size();
    }

    public class BidHistoryHolder extends RecyclerView.ViewHolder {
        AppCompatTextView name,price,time;
        ImageView call;

        public BidHistoryHolder(View itemView) {
            super(itemView);
            time =  itemView.findViewById(R.id.tv_bid_history_item_time);
            price =  itemView.findViewById(R.id.tv_bid_history_item_price);
            name =  itemView.findViewById(R.id.tv_bid_history_item_name);
            call =  itemView.findViewById(R.id.image_bid_history_item);



        }

  
    }

}
