package future.car.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import future.car.R;
import future.car.Utils.RecyclerViewClickListener;


/**
 * Created by Mohamed Magdy on 03/11/2017.
 */

public class PopUpAdapter extends RecyclerView.Adapter<PopUpAdapter.CarSaleHolder> {
    String  arr[];
    private Context context;
    private RecyclerViewClickListener itemListener;


    public PopUpAdapter(String arr[] , Context context, RecyclerViewClickListener itemListener) {
        this.arr = arr;
        this.context = context;
        this.itemListener = itemListener;

    }

    @Override
    public CarSaleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CarSaleHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.popup_item, parent, false));
    }

    @Override
    public void onBindViewHolder(CarSaleHolder holder, int position) {
        holder.name.setText(arr[position]);
    }

    public int getItemCount() {
        return arr.length;
    }

    public class CarSaleHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name;

        public CarSaleHolder(View itemView) {
            super(itemView);
            name =  itemView.findViewById(R.id.tv_popup_item);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v, this.getLayoutPosition());

        }
    }

}
