package future.car.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import future.car.Model.HomeModel;
import future.car.R;
import future.car.Utils.RecyclerViewClickListener;
import future.car.Utils.Tools;


/**
 * Created by Mohamed Magdy on 03/11/2017.
 */

public class CarAuctionHomeAdapter extends RecyclerView.Adapter<CarAuctionHomeAdapter.CarAuctionHolder> {
    public List<HomeModel.CarDetails> carAuctions;
    private Context context;
    private RecyclerViewClickListener itemListener;


    public CarAuctionHomeAdapter(List<HomeModel.CarDetails> carAuctions, Context context, RecyclerViewClickListener itemListener) {
        this.carAuctions = carAuctions;
        this.context = context;
        this.itemListener = itemListener;

    }

    @Override
    public CarAuctionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CarAuctionHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.car_item, parent, false));
    }

    @Override
    public void onBindViewHolder(CarAuctionHolder holder, int position) {
        HomeModel.CarDetails carAuction = carAuctions.get(position);

        holder.type.setText(context.getString(R.string.auction));
        holder.type.setBackgroundColor(context.getResources().getColor(R.color.green));
        holder.name.setText(carAuction.title);
        holder.price.setText(carAuction.price);
        holder.model.setText(carAuction.carBrand);
        holder.year.setText(carAuction.year);
        holder.kilometers.setText(carAuction.mileage);
        Tools.loadImage(holder.image,carAuction.image);

        if (Tools.getCurrentLanguage(context).equals("ar")){
            holder.name.setTextDirection(View.TEXT_DIRECTION_RTL);
            holder.price.setTextDirection(View.TEXT_DIRECTION_RTL);
            holder.model.setTextDirection(View.TEXT_DIRECTION_RTL);
            holder.year.setTextDirection(View.TEXT_DIRECTION_RTL);
            holder.kilometers.setTextDirection(View.TEXT_DIRECTION_RTL);
        }


    }

    public int getItemCount() {
        return carAuctions.size();
    }

    public class CarAuctionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name,price,model,year,kilometers,type;
        ImageView image;

        public CarAuctionHolder(View itemView) {
            super(itemView);
            image =  itemView.findViewById(R.id.image_car_item);
            name =  itemView.findViewById(R.id.tv_car_item_name);
            type =  itemView.findViewById(R.id.tv_car_item_type);
            price =  itemView.findViewById(R.id.tv_car_item_price);
            model =  itemView.findViewById(R.id.tv_car_item_model);
            year =  itemView.findViewById(R.id.tv_car_item_year);
            kilometers =  itemView.findViewById(R.id.tv_car_item_kilometers);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v, this.getLayoutPosition());

        }
    }

}
