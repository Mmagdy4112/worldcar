package future.car.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import future.car.Model.HomeModel;
import future.car.R;
import future.car.Utils.RecyclerViewClickListener;
import future.car.Utils.Tools;


/**
 * Created by Mohamed Magdy on 03/11/2017.
 */

public class AdsAdapter extends RecyclerView.Adapter<AdsAdapter.MyAdvHolder> {
    public List<HomeModel.Advertisement> advertisements;
    private Context context;
    private RecyclerViewClickListener itemListener;


    public AdsAdapter(List<HomeModel.Advertisement> advertisements, Context context, RecyclerViewClickListener itemListener) {
        this.advertisements = advertisements;
        this.context = context;
        this.itemListener = itemListener;

    }

    @Override
    public MyAdvHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyAdvHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ads_item, parent, false));
    }

    @Override
    public void onBindViewHolder(MyAdvHolder holder, int position) {
        HomeModel.Advertisement advertisement = advertisements.get(position);


//        Tools.loadImage(holder.image,"https://vignette.wikia.nocookie.net/undertale-au/images/5/54/Link.jpg/revision/latest?cb=20170903211129");
        Tools.loadImage(holder.image,advertisement.image);



    }

    public int getItemCount() {
        return advertisements.size();
    }

    public class MyAdvHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView image;

        public MyAdvHolder(View itemView) {
            super(itemView);
            image =  itemView.findViewById(R.id.image_ads_item);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v, this.getLayoutPosition());

        }
    }
}
