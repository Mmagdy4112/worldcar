package future.car.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import future.car.Model.MenuModel;
import future.car.R;
import future.car.Utils.SessionManager;

/**
 * Created by Mohamed Magdy on 18/02/2018.
 */

public class MenuAdapeter extends BaseAdapter {
    Context context;
    List<MenuModel> rowItems;
    SessionManager session;

    public MenuAdapeter(Context context, List<MenuModel> items) {
        this.context = context;
        this.rowItems = items;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtDesc;
        LinearLayout Container;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder ;



        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.menu_item, null);
            holder = new ViewHolder();
            holder.txtDesc = convertView.findViewById(R.id.tv_menu_item);
            holder.imageView =  convertView.findViewById(R.id.image_menu_item);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        MenuModel rowItem = (MenuModel) getItem(position);



        holder.txtDesc.setText(rowItem.getDesc());
        holder.imageView.setImageResource(rowItem.getImageId());

        return convertView;
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}