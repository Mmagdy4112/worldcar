package future.car.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import future.car.Model.HomeModel;
import future.car.R;
import future.car.Utils.RecyclerViewClickListener;
import future.car.Utils.Tools;


/**
 * Created by Mohamed Magdy on 03/11/2017.
 */

public class CarRentHomeAdapter extends RecyclerView.Adapter<CarRentHomeAdapter.CarRentHolder> {
    public List<HomeModel.CarDetails> carRents;
    private Context context;
    private RecyclerViewClickListener itemListener;


    public CarRentHomeAdapter(List<HomeModel.CarDetails> carRents, Context context, RecyclerViewClickListener itemListener) {
        this.carRents = carRents;
        this.context = context;
        this.itemListener = itemListener;

    }

    @Override
    public CarRentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CarRentHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.car_item, parent, false));
    }

    @Override
    public void onBindViewHolder(CarRentHolder holder, int position) {
        HomeModel.CarDetails carRent = carRents.get(position);

        holder.type.setText(context.getString(R.string.rent));
        holder.type.setBackgroundColor(context.getResources().getColor(R.color.yellow));
        holder.name.setText(carRent.title);
        holder.price.setText(carRent.price);
        holder.model.setText(carRent.carBrand);
        holder.year.setText(carRent.year);
        holder.kilometers.setText(carRent.mileage);
        Tools.loadImage(holder.image,carRent.image);

        if (Tools.getCurrentLanguage(context).equals("ar")){
            holder.name.setTextDirection(View.TEXT_DIRECTION_RTL);
            holder.price.setTextDirection(View.TEXT_DIRECTION_RTL);
            holder.model.setTextDirection(View.TEXT_DIRECTION_RTL);
            holder.year.setTextDirection(View.TEXT_DIRECTION_RTL);
            holder.kilometers.setTextDirection(View.TEXT_DIRECTION_RTL);
        }

    }

    public int getItemCount() {
        return carRents.size();
    }

    public class CarRentHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name,price,model,year,kilometers,type;
        ImageView image;

        public CarRentHolder(View itemView) {
            super(itemView);
            image =  itemView.findViewById(R.id.image_car_item);
            name =  itemView.findViewById(R.id.tv_car_item_name);
            type =  itemView.findViewById(R.id.tv_car_item_type);
            price =  itemView.findViewById(R.id.tv_car_item_price);
            model =  itemView.findViewById(R.id.tv_car_item_model);
            year =  itemView.findViewById(R.id.tv_car_item_year);
            kilometers =  itemView.findViewById(R.id.tv_car_item_kilometers);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v, this.getLayoutPosition());

        }
    }

}
