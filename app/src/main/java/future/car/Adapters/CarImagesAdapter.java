package future.car.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import future.car.R;
import future.car.Utils.RecyclerViewClickListener;
import future.car.Utils.Tools;


/**
 * Created by Mohamed Magdy on 03/11/2017.
 */

public class CarImagesAdapter extends RecyclerView.Adapter<CarImagesAdapter.CarImagesHolder> {
    public List<String> images;
    private Context context;
    private RecyclerViewClickListener itemListener;


    public CarImagesAdapter(List<String> images, Context context, RecyclerViewClickListener itemListener) {
        this.images = images;
        this.context = context;
        this.itemListener = itemListener;

    }

    @Override
    public CarImagesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CarImagesHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.car_images_item, parent, false));
    }

    @Override
    public void onBindViewHolder(CarImagesHolder holder, int position) {

        Tools.loadImage(holder.image,images.get(position));

    }

    public int getItemCount() {
        return images.size();
    }

    public class CarImagesHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView image;

        public CarImagesHolder(View itemView) {
            super(itemView);
            image =  itemView.findViewById(R.id.image_car_images_item);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v, this.getLayoutPosition());

        }
    }

}
