package future.car.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import future.car.Model.HomeModel;
import future.car.R;
import future.car.Utils.RecyclerViewClickListener;
import future.car.Utils.Tools;


/**
 * Created by Mohamed Magdy on 03/11/2017.
 */

public class MyAdvAdapter extends RecyclerView.Adapter<MyAdvAdapter.MyAdvHolder> {
    public List<HomeModel.CarDetails> carDetails;
    private Context context;
    private RecyclerViewClickListener itemListener;


    public MyAdvAdapter(List<HomeModel.CarDetails> carDetails, Context context, RecyclerViewClickListener itemListener) {
        this.carDetails = carDetails;
        this.context = context;
        this.itemListener = itemListener;

    }

    @Override
    public MyAdvHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyAdvHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sale_car_item, parent, false));
    }

    @Override
    public void onBindViewHolder(MyAdvHolder holder, int position) {
        HomeModel.CarDetails carDetail = carDetails.get(position);


        holder.name.setText(carDetail.title);
        holder.price.setText(carDetail.price);
        holder.details.setText(carDetail.title);
        holder.model.setText(context.getResources().getString(R.string.model)+" : "+carDetail.carBrand);
        holder.year.setText(context.getResources().getString(R.string.year)+" : "+carDetail.year);
        holder.kilometers.setText(context.getResources().getString(R.string.km)+" : "+carDetail.mileage);
        Tools.loadImage(holder.image,carDetail.image);



        if (Tools.getCurrentLanguage(context).equals("ar")){
            holder.details.setTextDirection(View.TEXT_DIRECTION_RTL);
            holder.name.setTextDirection(View.TEXT_DIRECTION_RTL);
            holder.price.setTextDirection(View.TEXT_DIRECTION_RTL);
            holder.model.setTextDirection(View.TEXT_DIRECTION_RTL);
            holder.year.setTextDirection(View.TEXT_DIRECTION_RTL);
            holder.kilometers.setTextDirection(View.TEXT_DIRECTION_RTL);
        }

    }

    public int getItemCount() {
        return carDetails.size();
    }

    public class MyAdvHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name,price,model,year,kilometers,details;
        ImageView image;

        public MyAdvHolder(View itemView) {
            super(itemView);
            image =  itemView.findViewById(R.id.image_sale_car_item);
            name =  itemView.findViewById(R.id.tv_sale_car_item_name);
            price =  itemView.findViewById(R.id.tv_sale_car_item_price);
            model =  itemView.findViewById(R.id.tv_sale_car_item_model);
            year =  itemView.findViewById(R.id.tv_sale_car_item_year);
            details =  itemView.findViewById(R.id.tv_sale_car_item_details);
            kilometers =  itemView.findViewById(R.id.tv_sale_car_item_kilometers);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v, this.getLayoutPosition());

        }
    }
}
