package future.car.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import future.car.Model.HomeModel;
import future.car.R;
import future.car.Utils.RecyclerViewClickListener;
import future.car.Utils.Tools;


/**
 * Created by Mohamed Magdy on 03/11/2017.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.CarRentHolder> {
    public List<HomeModel.CarDetails> cars;
    private Context context;
    private RecyclerViewClickListener itemListener;


    public SearchAdapter(List<HomeModel.CarDetails> cars, Context context, RecyclerViewClickListener itemListener) {
        this.cars = cars;
        this.context = context;
        this.itemListener = itemListener;

    }

    @Override
    public CarRentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CarRentHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_item, parent, false));
    }

    @Override
    public void onBindViewHolder(CarRentHolder holder, int position) {
        HomeModel.CarDetails car = cars.get(position);

        holder.name.setText(car.title);
        holder.price.setText(car.price);
        holder.model.setText(car.carBrand);
        holder.year.setText(car.year);
        holder.kilometers.setText(car.mileage);
        Tools.loadImage(holder.image, car.image);


        if (car.saleType.equals("buy")) {
            holder.type.setText(context.getString(R.string.buy));
            holder.type.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        } else if (car.saleType.equals("rent")) {

            holder.type.setText(context.getString(R.string.rent));
            holder.type.setBackgroundColor(context.getResources().getColor(R.color.yellow));
        } else if (car.saleType.equals("auction")) {

            holder.type.setText(context.getString(R.string.auction));
            holder.type.setBackgroundColor(context.getResources().getColor(R.color.green));
        }


        if (Tools.getCurrentLanguage(context).equals("ar")){
            holder.name.setTextDirection(View.TEXT_DIRECTION_RTL);
            holder.price.setTextDirection(View.TEXT_DIRECTION_RTL);
            holder.model.setTextDirection(View.TEXT_DIRECTION_RTL);
            holder.year.setTextDirection(View.TEXT_DIRECTION_RTL);
            holder.kilometers.setTextDirection(View.TEXT_DIRECTION_RTL);
        }
    }

    public int getItemCount() {
        return cars.size();
    }

    public class CarRentHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name, price, model, year, kilometers, type;
        ImageView image;

        public CarRentHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image_search_item);
            name = itemView.findViewById(R.id.tv_search_item_name);
            type = itemView.findViewById(R.id.tv_search_item_type);
            price = itemView.findViewById(R.id.tv_search_item_price);
            model = itemView.findViewById(R.id.tv_search_item_model);
            year = itemView.findViewById(R.id.tv_search_item_year);
            kilometers = itemView.findViewById(R.id.tv_search_item_kilometers);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v, this.getLayoutPosition());

        }
    }

}
