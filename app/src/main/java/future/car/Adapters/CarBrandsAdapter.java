package future.car.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import future.car.Model.BrandsModel;
import future.car.R;
import future.car.Utils.RecyclerViewClickListener;
import future.car.Utils.Tools;


/**
 * Created by Mohamed Magdy on 03/11/2017.
 */

public class CarBrandsAdapter extends RecyclerView.Adapter<CarBrandsAdapter.CarSaleHolder> {
    public List<BrandsModel.CarBrands> carBrands;
    private Context context;
    private RecyclerViewClickListener itemListener;


    public CarBrandsAdapter(List<BrandsModel.CarBrands> carBrands, Context context, RecyclerViewClickListener itemListener) {
        this.carBrands = carBrands;
        this.context = context;
        this.itemListener = itemListener;

    }

    @Override
    public CarSaleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CarSaleHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_item, parent, false));
    }

    @Override
    public void onBindViewHolder(CarSaleHolder holder, int position) {
        BrandsModel.CarBrands carBrand = carBrands.get(position);
        holder.name.setText(carBrand.brandName);
        Tools.loadImage(holder.image,carBrand.brandLogo);
    }

    public int getItemCount() {
        return carBrands.size();
    }

    public class CarSaleHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name;
        ImageView image;

        public CarSaleHolder(View itemView) {
            super(itemView);
            image =  itemView.findViewById(R.id.image_category_item);
            name =  itemView.findViewById(R.id.tv_category_item);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v, this.getLayoutPosition());

        }
    }

}
