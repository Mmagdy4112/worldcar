package future.car.GifHandle;

import android.content.Context;
import android.webkit.WebView;

/**
 * Created by Mohamed Magdy on 20/01/2018.
 */

public class GifWebView extends WebView {

    public GifWebView(Context context, String path) {
        super(context);

        loadUrl(path);
    }
}