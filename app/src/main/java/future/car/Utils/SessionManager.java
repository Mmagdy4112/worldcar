package future.car.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;


/**
 * Created by grand on 06/03/2017.
 */

public class SessionManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "UserData";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    // User name (make variable public to access from outside)

    // Email address (make variable public to access from outside)
    public static final String NAME  = "name";
    public static final String USER_ID = "user_id";
    public static final String EMAIL = "email";
    public static final String GENDER = "gender";
    public static final String TYPE = "type";
    public static final String TOKEN = "token";
    public static final String COUNTRY_ID = "countryId";
    public static final String COUNTRY_NAME = "countryName";
    public static final String IMAGE = "image";
    // Constructor
    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
    public void createLoginSession(String id, String name, String email , String gender, String type
            , String token ,String countryName, String countryId,String image){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(USER_ID, id);
        editor.putString(NAME, name);
        editor.putString(EMAIL, email);
        editor.putString(GENDER, gender);
        editor.putString(TYPE, type);
        editor.putString(TOKEN, token);
        editor.putString(COUNTRY_ID, countryName);
        editor.putString(COUNTRY_NAME, countryId);
        editor.putString(IMAGE, image);
        editor.commit();
    }


    public String getToken(){
        return  _context.getSharedPreferences(PREF_NAME,PRIVATE_MODE).getString(TOKEN,"0");
    }

    public String getId(){
        return  _context.getSharedPreferences(PREF_NAME,PRIVATE_MODE).getString(USER_ID,"");
    }
    public String getEmail(){
        return  _context.getSharedPreferences(PREF_NAME,PRIVATE_MODE).getString(EMAIL,"");
    }
    public void checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
           /* Intent i = new Intent(_context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);*/
        }

    }



    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();

        user.put(USER_ID, pref.getString(USER_ID, ""));
        user.put(NAME, pref.getString(NAME, ""));
        user.put(EMAIL, pref.getString(EMAIL, ""));
        user.put(GENDER, pref.getString(GENDER, ""));
        user.put(TYPE, pref.getString(TYPE, ""));
        user.put(TOKEN, pref.getString(TOKEN, ""));
        user.put(COUNTRY_NAME, pref.getString(COUNTRY_NAME, ""));
        user.put(COUNTRY_ID, pref.getString(COUNTRY_ID, ""));
        user.put(IMAGE, pref.getString(IMAGE, ""));

        // return user
        return user;
    }

    /**
     * Clear session details
     * */
    public void logoutUser(){
        editor.clear();
        editor.commit();
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }

}
