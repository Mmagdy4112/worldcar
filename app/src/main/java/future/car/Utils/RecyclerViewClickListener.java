package future.car.Utils;

import android.view.View;

/**
 * Created by mac on 5/21/17.
 */

public interface RecyclerViewClickListener {
    public void recyclerViewListClicked(View v, int position);

}
