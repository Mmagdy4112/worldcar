package future.car.Utils;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import future.car.Activities.SignIn;
import future.car.R;


/**
 * Created by mac on 7/27/17.
 */
public class Tools {
    public static DisplayImageOptions options = new DisplayImageOptions.Builder().showImageForEmptyUri(R.color.colorAccent).showImageOnLoading(R.color.colorAccent).showImageOnFail(R.color.colorAccent).cacheInMemory(true).cacheOnDisk(true).build();

    public static String link = "";
    public static String filesLink = "";
    public static ImageLoader imageLoader = ImageLoader.getInstance();



    public static void setHeaderFooterColor(Context context) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = ((Activity) context).getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(context.getResources().getColor(R.color.colorAccent));
                window.setNavigationBarColor(context.getResources().getColor(R.color.colorAccent));
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
    }
    public static void makeCall(Context context,String number)
    {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number));
        context.startActivity(intent);
    }




  /*  public static void addFragment(Context context, Fragment fragment, String backStackText) {
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().add(R.id.content, fragment);
        if (!backStackText.equals("")) {
            fragmentTransaction.addToBackStack(backStackText);
        }
        fragmentTransaction.commit();
    }


    public static void replaceFragment(Context context, Fragment fragment, String backStackText) {
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.content, fragment);
        if (!backStackText.equals("")) {
            fragmentTransaction.addToBackStack(backStackText);
        }
        fragmentTransaction.commit();
    }

    public static void serverResultActions(Context context, VolleyError volleyError) {
        if (volleyError instanceof ServerError) {
            MUT.lToast(context, context.getResources().getString(R.string.server_error));
        } else if (volleyError instanceof ParseError) {
            MUT.lToast(context, context.getString(R.string.no_date));
        } else if (volleyError instanceof NoConnectionError || volleyError instanceof TimeoutError) {
            MUT.lToast(context, context.getResources().getString(R.string.connection_error));
        } else {
            MUT.lToast(context, context.getResources().getString(R.string.server_error));
        }

    }*/



    public static Uri specialCameraSelector(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 70, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, new Date(System.currentTimeMillis()).toString()+"photo", null);
        return Uri.parse(path);
    }

    public static SharedPreferences getUserDetails(Context context) {
        return context.getSharedPreferences("userDetails", context.MODE_PRIVATE);
    }

    public static String getCurrentLanguage(Context context) {
        return context.getSharedPreferences("language", context.MODE_PRIVATE).getString("language", "en");
    }

    public static void loadImage(final ImageView image, String path) {

        imageLoader.displayImage(filesLink + path, image,  options);
    }



    public static void changeLanguage(String languageToLoad, Context context) {

        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());
        SharedPreferences languagepref = context.getSharedPreferences("language", context.MODE_PRIVATE);
        SharedPreferences.Editor editor = languagepref.edit();
        editor.putString("language", languageToLoad);
        editor.commit();

    }


    public static String getDeviceId(Activity context) {
        final TelephonyManager tm = (TelephonyManager) context.getBaseContext()
                .getSystemService(Context.TELEPHONY_SERVICE);
        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = ""
                + android.provider.Settings.Secure.getString(
                context.getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID);
        UUID deviceUuid = new UUID(androidId.hashCode(),
                ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        return deviceUuid.toString();

    }
    public static boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }





    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay()
                .getMetrics(displayMetrics);
        view.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels,
                displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
                view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }





    public static  void setRatingBarColors(Context context, RatingBar ratingBar) {
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(context.getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(context.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(context.getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
    }

    public static boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


    public static void setRVHVertical(Context context,RecyclerView recyclerView){
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, true));
    }

    public static void setRelativeViewSize(View view , int width , int height){
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width, height);
        view.setLayoutParams(layoutParams);
    }

    public static void setLinearViewSize(View view , int width , int height){
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, height);
        view.setLayoutParams(layoutParams);
    }


    public static float dpFromPx(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    public static void noConnection(Context context, View coordinatorLayout, final NoConnection noConnection) {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "" + context.getResources().getString(R.string.CheckInternet), Snackbar.LENGTH_INDEFINITE)
                .setAction("" + context.getResources().getString(R.string.retry), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        noConnection.passedMethod();
                    }
                });

        snackbar.setActionTextColor(Color.CYAN);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        textView.setTextDirection(View.TEXT_DIRECTION_LTR);
        snackbar.show();
    }


    public static void startActivity(Context context, Class<?> activity)
    {
        Intent intent = new Intent(context,activity);
        context.startActivity(intent);
    }
    public static String getShared(Context context ,  String prefKey) {
        return context.getSharedPreferences("shared", Context.MODE_PRIVATE).getString(prefKey, "");
    }


    public static void setShared(Context context , String prefKey ,String prefValue) {
        context.getSharedPreferences("shared", Context.MODE_PRIVATE).edit().putString(prefKey, prefValue).apply();
    }

    public static Dialog createLoadingBar(Context context) {
        Dialog dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_bar);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    public static Dialog loginPopUp(final Context context) {
        AppCompatButton btnLogin,btnCancel;
        View view = LayoutInflater.from(context).inflate(
                R.layout.login_popup, null);
        final Dialog dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        btnLogin = view.findViewById(R.id.btn_login_popup_login);
        btnCancel = view.findViewById(R.id.btn_login_popup_cancel);




        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent i =new Intent(context,SignIn.class);
                context.startActivity(i);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        return dialog;
    }


    public static void storeArayList(Context context, Object objects,String prefKey) {
        Gson gson = new Gson();
        String jsonText = gson.toJson(objects);
        setShared(context, prefKey, jsonText);
    }



    public static Object getObjList(Context context, String prefKey, Type type) {
        String list = getShared(context, prefKey);
        return new Gson().fromJson(list, type);
    }
}
