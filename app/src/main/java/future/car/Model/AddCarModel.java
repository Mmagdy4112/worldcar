package future.car.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Mohamed Magdy on 23/03/2018.
 */

public class AddCarModel implements Serializable {

    @SerializedName("cars")
    @Expose
    public String cars;
    @SerializedName("status")
    @Expose
    public int status;
    @SerializedName("sale_type")
    @Expose
    public String saleType;
    @SerializedName("brand_id")
    @Expose
    public Integer brandId;
    @SerializedName("type_id")
    @Expose
    public Integer typeId;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("start_bid")
    @Expose
    public String startBid;
    @SerializedName("minimum_bid")
    @Expose
    public String minimumBid;
    @SerializedName("bid_time")
    @Expose
    public Integer bidTime;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("country_id")
    @Expose
    public Integer countryId;
    @SerializedName("city_id")
    @Expose
    public Integer cityId;
    @SerializedName("year")
    @Expose
    public Integer year;
    @SerializedName("mileage")
    @Expose
    public Integer mileage;
    @SerializedName("origin")
    @Expose
    public String origin;
    @SerializedName("exterior_color_id")
    @Expose
    public Integer exteriorColor;
    @SerializedName("interior_color_id")
    @Expose
    public Integer interiorColor;
    @SerializedName("cylinders_number")
    @Expose
    public Integer cylindersNumber;
    @SerializedName("bedding_type_id")
    @Expose
    public Integer beddingType;
    @SerializedName("gearbox")
    @Expose
    public String gearbox;
    @SerializedName("pushing_type")
    @Expose
    public String pushingType;
    @SerializedName("camera")
    @Expose
    public String camera;
    @SerializedName("sunroof")
    @Expose
    public String sunroof;
    @SerializedName("sensors")
    @Expose
    public String sensors;
    @SerializedName("bluetooth")
    @Expose
    public String bluetooth;
    @SerializedName("map_system")
    @Expose
    public String mapSystem;
    @SerializedName("notes")
    @Expose
    public String notes;
    @SerializedName("car_images")
    @Expose
    public String carImages[] = null;
    private final static long serialVersionUID = -2376959389296656811L;

}
