package future.car.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Mohamed Magdy on 23/03/2018.
 */

public class BrandsModel implements Serializable {

    @SerializedName("status")
    @Expose
    public Integer status;
    @SerializedName("car_brands")
    @Expose
    public List<CarBrands> carBrands = null;


    public class CarBrands implements Serializable {
        @SerializedName("brand_id")
        @Expose
        public Integer brandId;
        @SerializedName("brand_name")
        @Expose
        public String brandName;
        @SerializedName("brand_logo")
        @Expose
        public String brandLogo;
        @SerializedName("carTypes")
        @Expose
        public List<HomeModel.CarType> carTypes = null;

    }
    public class CarType implements Serializable {

        private final static long serialVersionUID = -8592510015844217300L;
        @SerializedName("type_id")
        @Expose
        public Integer typeId;
        @SerializedName("type_name")
        @Expose
        public String typeName;

    }
}
