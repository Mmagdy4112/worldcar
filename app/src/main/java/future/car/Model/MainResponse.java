package future.car.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mohamed Magdy on 21/12/2017.
 */

public class MainResponse {
    @SerializedName("cars")
    @Expose
    public String cars;

    @SerializedName("errors")
    @Expose
    public String errors [] = null;

    @SerializedName("status")
    @Expose
    public int status;
    @SerializedName("message")
    @Expose
    public int message;
    @SerializedName("car_id")
    @Expose
    public int carId;
    @SerializedName("car_bid")
    @Expose
    public String carBid;
}
