package future.car.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Mohamed Magdy on 17/02/2018.
 */

public class HomeModel implements Serializable {
    @SerializedName("status")
    @Expose
    public Integer status;
    @SerializedName("car_buy")
    @Expose
    public List<CarDetails> carBuy = null;
    @SerializedName("car_rent")
    @Expose
    public List<CarDetails> carRent = null;
    @SerializedName("car_brand")
    @Expose
    public List<CarDetails> carBrand = null;
    @SerializedName("car_auction")
    @Expose
    public List<CarDetails> carAuction = null;
    @SerializedName("cars")
    @Expose
    public List<CarDetails> cars = null;
    @SerializedName("car_seller")
    @Expose
    public List<CarDetails> carSeller = null;
    @SerializedName("advertisements")
    @Expose
    public List<Advertisement> advertisements = null;
    @SerializedName("paid_advertisements")
    @Expose
    public List<PaidAdvertisement> paidAdvertisements = null;

    public class PaidAdvertisement implements Serializable
    {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("image")
        @Expose
        public String image;
        @SerializedName("link")
        @Expose
        public String link;
        private final static long serialVersionUID = -6048835518768103330L;

    }

    public class CarDetails implements Serializable {

        private final static long serialVersionUID = 8884678072840817097L;
        @SerializedName("car_id")
        @Expose
        public Integer carId;
        @SerializedName("car_brand")
        @Expose
        public String carBrand;
        @SerializedName("notes")
        @Expose
        public String notes;
        @SerializedName("car_type")
        @Expose
        public String carType;
        @SerializedName("country")
        @Expose
        public String country;
        @SerializedName("city")
        @Expose
        public String city;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("image")
        @Expose
        public String image;
        @SerializedName("price")
        @Expose
        public String price;
        @SerializedName("year")
        @Expose
        public String year;
        @SerializedName("mileage")
        @Expose
        public String mileage;
        @SerializedName("origin")
        @Expose
        public String origin;
        @SerializedName("bid_time_left")
        @Expose
        public String bidTimeLeft;
        @SerializedName("bid_end_time")
        @Expose
        public String bidEndTime;
        @SerializedName("number_of_bidding")
        @Expose
        public String numberOfBidding;
        @SerializedName("exterior_color")
        @Expose
        public String exteriorColor;
        @SerializedName("interior_color")
        @Expose
        public String interiorColor;
        @SerializedName("cylinders_number")
        @Expose
        public String cylindersNumber;
        @SerializedName("bedding_type")
        @Expose
        public String beddingType;
        @SerializedName("sale_type")
        @Expose
        public String saleType;
        @SerializedName("gearbox")
        @Expose
        public String gearbox;
        @SerializedName("pushing_type")
        @Expose
        public String pushingType;
        @SerializedName("camera")
        @Expose
        public String camera;
        @SerializedName("sunroof")
        @Expose
        public String sunroof;
        @SerializedName("sensors")
        @Expose
        public String sensors;
        @SerializedName("bluetooth")
        @Expose
        public String bluetooth;
        @SerializedName("map_system")
        @Expose
        public String mapSystem;
        @SerializedName("notes_ar")
        @Expose
        public String notesAr;
        @SerializedName("notes_en")
        @Expose
        public String notesEn;
        @SerializedName("car_images")
        @Expose
        public List<String> carImages = null;
        @SerializedName("start_bid")
        @Expose
        public String startBid;
        @SerializedName("minimum_bid")
        @Expose
        public String minimumBid;
        @SerializedName("bid_time")
        @Expose
        public String bidTime;
        @SerializedName("user_name")
        @Expose
        public String userName;
        @SerializedName("user_email")
        @Expose
        public String userEmail;
        @SerializedName("user_mobile")
        @Expose
        public String userMobile;
        @SerializedName("user_id")
        @Expose
        public int userId;



    }

    public static class CarBrand implements Serializable
    {

        @SerializedName("brand_id")
        @Expose
        public Integer brandId;
        @SerializedName("brand_name")
        @Expose
        public String brandName;
        @SerializedName("brand_logo")
        @Expose
        public String brandLogo;
        @SerializedName("carTypes")
        @Expose
        public List<CarType> carTypes = null;
        private final static long serialVersionUID = 2803200015586453203L;


    }
    public class CarType implements Serializable
    {

        @SerializedName("type_id")
        @Expose
        public Integer typeId;
        @SerializedName("type_name")
        @Expose
        public String typeName;
        private final static long serialVersionUID = -8592510015844217300L;

    }

    public class Advertisement implements Serializable
    {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("image")
        @Expose
        public String image;
        @SerializedName("link")
        @Expose
        public String link;
        private final static long serialVersionUID = -5416935198725549416L;

    }
}
