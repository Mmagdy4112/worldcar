package future.car.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MetaDataModel implements Serializable {

    @SerializedName("status")
    @Expose
    public Integer status;
    @SerializedName("data")
    @Expose
    public Data data;
    private final static long serialVersionUID = 7495084450190793877L;

    public class Data implements Serializable {

        @SerializedName("bank_accounts")
        @Expose
        public List<BankAccount> bankAccounts = null;
        @SerializedName("exterior_colors")
        @Expose
        public List<ExteriorColor> exteriorColors = null;
        @SerializedName("beddingTypes")
        @Expose
        public List<BeddingTypes> beddingTypes = null;
        @SerializedName("interior_colors")
        @Expose
        public List<ExteriorColor> interiorColors = null;
        @SerializedName("countries")
        @Expose
        public List<Countries> countries = null;
        @SerializedName("mobiles")
        @Expose
        public Mobiles mobiles;
        @SerializedName("emails")
        @Expose
        public Emails emails;
        private final static long serialVersionUID = -1621639661104955348L;

    }

    public class Emails implements Serializable {

        @SerializedName("email_1")
        @Expose
        public String email1;
        @SerializedName("email_2")
        @Expose
        public String email2;
        private final static long serialVersionUID = -8056372829944456859L;

    }

    public class Countries implements Serializable {

        @SerializedName("id")
        @Expose
        public int id;
        @SerializedName("name")
        @Expose
        public String name;
        private final static long serialVersionUID = -8056372829944456859L;

    }

    public class Mobiles implements Serializable {

        @SerializedName("mobile_1")
        @Expose
        public String mobile1;
        @SerializedName("mobile_2")
        @Expose
        public String mobile2;
        @SerializedName("mobile_3")
        @Expose
        public String mobile3;
        private final static long serialVersionUID = 6926777572849629375L;

    }

    public class BankAccount implements Serializable {

        @SerializedName("bank_name")
        @Expose
        public String bankNameAr;
        @SerializedName("owner_name")
        @Expose
        public String ownerNameAr;
        @SerializedName("account_number")
        @Expose
        public String accountNumber;
        @SerializedName("ipan_number")
        @Expose
        public String ipanNumber;
        private final static long serialVersionUID = 4827369812548428356L;

    }

    public class ExteriorColor implements Serializable {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("color")
        @Expose
        public String color;
        private final static long serialVersionUID = -6517556980289589114L;

    }

    public class BeddingTypes implements Serializable {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("type")
        @Expose
        public String name;
        private final static long serialVersionUID = -6517556980289589114L;

    }

}