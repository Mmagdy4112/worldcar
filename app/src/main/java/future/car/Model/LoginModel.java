package future.car.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Mohamed Magdy on 22/01/2018.
 */

public class LoginModel {

    @SerializedName("status")
    @Expose
    public int status ;
    @SerializedName("password")
    @Expose
    public String password;
    @SerializedName("errors")
    @Expose
    public List<String> errors = null;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("mobile")
    @Expose
    public String mobile;
    @SerializedName("user")
    @Expose
    public User user = null;
    @SerializedName("old_password")
    @Expose
    public String oldPassword;
    @SerializedName("password_confirmation")
    @Expose
    public String passwordConfirmation;

    @SerializedName("first_name")
    @Expose
    public String first_name;
    @SerializedName("last_name")
    @Expose
    public String last_name;
    @SerializedName("country_id")
    @Expose
    public String country_id;
    @SerializedName("gender")
    @Expose
    public String gender;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("image")
    @Expose
    public String image;
    public class User implements Serializable {
        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("image")
        @Expose
        public String image;
        @SerializedName("first_name")
        @Expose
        public String firstName;
        @SerializedName("last_name")
        @Expose
        public String lastName;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("gender")
        @Expose
        public String gender;
        @SerializedName("type")
        @Expose
        public String type;
        @SerializedName("token")
        @Expose
        public String token;
        @SerializedName("user_id")
        @Expose
        public String userId;
    }
}
