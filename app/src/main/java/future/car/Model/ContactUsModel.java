package future.car.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ContactUsModel implements Serializable
{

    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("message")
    @Expose
    public String message;
    private final static long serialVersionUID = -2101518367999898918L;

}