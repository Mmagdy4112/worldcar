package future.car.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mohamed Magdy on 22/01/2018.
 */

public class CountriesModel {
    @SerializedName("status")
    @Expose
    public Integer status;
    @SerializedName("countries")
    @Expose
    public List<Country> countries = null;

    public class Country {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("name_en")
        @Expose
        public String nameEn;
        @SerializedName("name_ar")
        @Expose
        public String nameAr;

    }
}
