package future.car.Model;

/**
 * Created by Mohamed Magdy on 18/02/2018.
 */

public class MenuModel {
    private int imageId;
    private String desc;


    public MenuModel(int imageId, String desc) {
        this.imageId = imageId;
        this.desc = desc;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return desc;
    }
}
