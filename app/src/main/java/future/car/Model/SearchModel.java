package future.car.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Mohamed Magdy on 30/03/2018.
 */

public class SearchModel implements Serializable {


    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("brand_id")
    @Expose
    public Integer brandId;
    @SerializedName("year")
    @Expose
    public Integer year;
    @SerializedName("price_from")
    @Expose
    public Integer priceFrom;
    @SerializedName("price_to")
    @Expose
    public Integer priceTo;
    @SerializedName("status")
    @Expose
    public Integer status;
    @SerializedName("interior_color")
    @Expose
    public String interiorColor;
    @SerializedName("exterior_color")
    @Expose
    public String exteriorColor;
    @SerializedName("gearbox")
    @Expose
    public String gearbox;
    @SerializedName("cylinders_number")
    @Expose
    public Integer cylindersNumber;
    @SerializedName("mileage")
    @Expose
    public Integer mileage;
    @SerializedName("cars")
    @Expose
    public List<Car> cars = null;



    public class Car implements Serializable
    {

        @SerializedName("car_id")
        @Expose
        public Integer carId;
        @SerializedName("car_brand")
        @Expose
        public String carBrand;
        @SerializedName("car_type")
        @Expose
        public String carType;
        @SerializedName("country")
        @Expose
        public String country;
        @SerializedName("city")
        @Expose
        public String city;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("image")
        @Expose
        public String image;
        @SerializedName("price")
        @Expose
        public String price;
        @SerializedName("year")
        @Expose
        public String year;
        @SerializedName("mileage")
        @Expose
        public String mileage;
        @SerializedName("origin")
        @Expose
        public String origin;
        @SerializedName("exterior_color")
        @Expose
        public String exteriorColor;
        @SerializedName("interior_color")
        @Expose
        public String interiorColor;
        @SerializedName("cylinders_number")
        @Expose
        public String cylindersNumber;
        @SerializedName("bedding_type")
        @Expose
        public String beddingType;
        @SerializedName("sale_type")
        @Expose
        public String saleType;
        @SerializedName("gearbox")
        @Expose
        public String gearbox;
        @SerializedName("pushing_type")
        @Expose
        public String pushingType;
        @SerializedName("camera")
        @Expose
        public String camera;
        @SerializedName("sunroof")
        @Expose
        public String sunroof;
        @SerializedName("sensors")
        @Expose
        public String sensors;
        @SerializedName("bluetooth")
        @Expose
        public String bluetooth;
        @SerializedName("map_system")
        @Expose
        public String mapSystem;
        @SerializedName("notes_ar")
        @Expose
        public String notesAr;
        @SerializedName("car_images")
        @Expose
        public List<String> carImages = null;
        private final static long serialVersionUID = -3260716555936790766L;

    }
}
