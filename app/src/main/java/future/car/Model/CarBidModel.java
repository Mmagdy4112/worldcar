package future.car.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CarBidModel implements Serializable {

    @SerializedName("car_id")
    @Expose
    public Integer car_id;
    @SerializedName("price")
    @Expose
    public Integer price;
    @SerializedName("status")
    @Expose
    public Integer status;
    @SerializedName("car_bid")
    @Expose
    public CarBid carBid;
    @SerializedName("bidders")
    @Expose
    public String bidders;
    private final static long serialVersionUID = 6078110289971812211L;

    public class CarBid implements Serializable {
        @SerializedName("bids")
        @Expose
        public List<Bid> bids = null;
        @SerializedName("bidders")
        @Expose
        public Integer bidders;
        @SerializedName("high_price")
        @Expose
        public Integer highPrice;


    }

    public class Bid implements Serializable {

        @SerializedName("mobile")
        @Expose
        public String mobile;
        @SerializedName("first_name")
        @Expose
        public String firstName;
        @SerializedName("last_name")
        @Expose
        public String lastName;
        @SerializedName("user_id")
        @Expose
        public Integer userId;
        @SerializedName("price")
        @Expose
        public Integer price;
        @SerializedName("created_at")
        @Expose
        public String createdAt;

        private final static long serialVersionUID = 1191632568230840214L;

    }
}