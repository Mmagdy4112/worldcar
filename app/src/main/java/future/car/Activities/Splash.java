package future.car.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import future.car.Model.HomeModel;
import future.car.R;
import future.car.Utils.SessionManager;
import future.car.Utils.Tools;
import future.car.WebServices.Webservice;
import io.michaelrocks.paranoid.Obfuscate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
@Obfuscate
public class Splash extends AppCompatActivity {
    ImageView splash;
    RelativeLayout mainLayout;
    SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Tools.changeLanguage(Locale.getDefault().getLanguage(), this);
        sessionManager = new SessionManager(this);
        splash = findViewById(R.id.image_splash);
        mainLayout = findViewById(R.id.splash);
        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(splash);
        Glide.with(this).load(R.raw.splash).into(imageViewTarget);
        if (Tools.haveNetworkConnection(this)) {

            if (sessionManager.isLoggedIn()) {

                Thread timer = new Thread() {
                    public void run() {
                        try {
                            sleep(3000);
                            getHomeData();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                };
                timer.start();
            } else {
                Thread timer = new Thread() {
                    public void run() {
                        try {
                            sleep(5000);
                            Intent i = new Intent(Splash.this, Intro.class);
                            finish();
                            startActivity(i);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                };
                timer.start();
            }

        } else {
            Intent i = new Intent(Splash.this, NoConnection.class);
            startActivity(i);
        }

    }

    private void getHomeData() {
        final String lang = Tools.getCurrentLanguage(this);
        Map<String, String> params = new HashMap<>();
        params.put("Content-Language", lang);

        Webservice.getInstance().getApi().home(params).enqueue(new Callback<HomeModel>() {
            @Override
            public void onResponse(Call<HomeModel> call, Response<HomeModel> response) {
                if (response.body() != null) {
                    Intent i = new Intent(Splash.this, Home.class);
                    i.putExtra("homeModel", response.body());
                    finish();
                    startActivity(i);

                }
            }

            @Override
            public void onFailure(Call<HomeModel> call, Throwable t) {
                Tools.noConnection(Splash.this, mainLayout, new future.car.Utils.NoConnection() {
                    @Override
                    public void passedMethod() {
                        getHomeData();
                    }
                });

            }
        });
    }
}
