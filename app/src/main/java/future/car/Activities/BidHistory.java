package future.car.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import java.util.List;

import future.car.Adapters.BidHistoryAdapter;
import future.car.Model.CarBidModel;
import future.car.R;
import future.car.Utils.Tools;
import io.michaelrocks.paranoid.Obfuscate;

@Obfuscate
public class BidHistory extends AppCompatActivity {

    AppCompatTextView tvBidders, tvBids, tvTimeLeft, tvDuration;
    ImageView back;
    RecyclerView rcvData;
    BidHistoryAdapter bidHistoryAdapter;
    List<CarBidModel.Bid> carBids;
    boolean bidEnd= false, carOwner= false;
    String userId="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bid_history);
        initializeDesign();

    }

    private void initializeDesign() {
        carBids = (List<CarBidModel.Bid>) getIntent().getSerializableExtra("carBids");
        back = findViewById(R.id.image_bid_history_back);
        tvBidders = findViewById(R.id.tv_bid_history_bidders);
        tvBids = findViewById(R.id.tv_bid_history_bids);
        tvTimeLeft = findViewById(R.id.tv_bid_history_time_left);
        tvDuration = findViewById(R.id.tv_bid_history_duration);
        rcvData = findViewById(R.id.rcv_bid_history);
        rcvData.setLayoutManager(new LinearLayoutManager(this));
        clickLusteners();
        if (Tools.getCurrentLanguage(this).equals("ar")) {
            back.setRotation(180);
        }

        bidEnd = getIntent().getExtras().getBoolean("bidEnd",false);
        carOwner = getIntent().getExtras().getBoolean("carOwner",false);

        setData();
    }


    private void clickLusteners() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void setData() {
        tvBids.setText(getResources().getString(R.string.bids)+" "+carBids.size());
        tvTimeLeft.setText(getResources().getString(R.string.time_left)+" "+getIntent().getExtras().getString("bidTimeLeft",""));
        tvDuration.setText(getResources().getString(R.string.duration)+" "+getIntent().getExtras().getString("bidEndTime",""));
        tvDuration.setVisibility(View.GONE);
        tvBidders.setText(getResources().getString(R.string.bidders)+" "+getIntent().getExtras().getInt("bidders",0));

        bidHistoryAdapter = new BidHistoryAdapter(carBids,this,carOwner);
        rcvData.setAdapter(bidHistoryAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
