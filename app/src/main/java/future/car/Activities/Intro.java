package future.car.Activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import future.car.Model.HomeModel;
import future.car.R;
import future.car.Utils.Tools;
import future.car.WebServices.Webservice;
import io.michaelrocks.paranoid.Obfuscate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
@Obfuscate
public class Intro extends AppCompatActivity {
    AppCompatTextView signUp,signIn,withOutLogin;
    LinearLayout mainLayout;
    String[] permissions = new String[]{
            Manifest.permission.CALL_PHONE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE};
    public static final int MULTIPLE_PERMISSIONS = 10;
    Dialog dialog ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        initializeDesign();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermissions();

        }
    }

    private void initializeDesign(){
        signUp = findViewById(R.id.tv_intro_signup);
        signIn = findViewById(R.id.tv_intro_signin);
        withOutLogin = findViewById(R.id.tv_intro_goto_home);
        mainLayout = findViewById(R.id.ll_intro);
        clickListeners();
    }
    private void clickListeners(){
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Tools.startActivity(Intro.this,SignIn.class);
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Tools.startActivity(Intro.this,SignUp.class);
            }
        });
        withOutLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHomeData();
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
            return false;
        } else {

            return true;

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    checkPermissions();
                }
                return;
            }
        }
    }
    private void getHomeData(){
        dialog = Tools.createLoadingBar(this);
        dialog.show();
        final String lang=Tools.getCurrentLanguage(this);
        Map<String, String> params = new HashMap<>();
        params.put("Content-Language", lang);

        Webservice.getInstance().getApi().home(params).enqueue(new Callback<HomeModel>() {
            @Override
            public void onResponse(Call<HomeModel> call, Response<HomeModel> response) {
                if (response.body()!=null){
                    Intent i = new Intent(Intro.this,Home.class);
                    i.putExtra("homeModel",response.body());
                    finish();
                    startActivity(i);

                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<HomeModel> call, Throwable t) {
                dialog.dismiss();
                Tools.noConnection(Intro.this, mainLayout, new future.car.Utils.NoConnection() {
                    @Override
                    public void passedMethod() {
                        getHomeData();
                    }
                });

            }
        });
    }
}
//    private void initializeDesign(){
//        clickListeners();
//    }
//    private void clickListeners(){}