package future.car.Activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import future.car.Model.BrandsModel;
import future.car.R;
import future.car.Utils.Tools;
import io.michaelrocks.paranoid.Obfuscate;

@Obfuscate
public class AddAdvertisingChoose extends AppCompatActivity {
    LinearLayout llBuy, llRent, llAuction;
    TextView tvBuy, tvRent, tvAuction;
    ImageView ivBuy, ivRent, ivAuction, back;
    Button choose;
    String selected = "";
    BrandsModel brandsModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_advertising_choose);
        initializeDesign();
    }

    private void initializeDesign() {
        choose = findViewById(R.id.btn_add_adv_choose);
        back = findViewById(R.id.image_add_adv_choose_back);
        ivAuction = findViewById(R.id.image_add_adv_choose_auction);
        ivRent = findViewById(R.id.image_add_adv_choose_rent_car);
        ivBuy = findViewById(R.id.image_add_adv_choose_buy_car);
        tvAuction = findViewById(R.id.tv_add_adv_choose_auction);
        tvRent = findViewById(R.id.tv_add_adv_choose_rent_car);
        tvBuy = findViewById(R.id.tv_add_adv_choose_buy_car);
        llAuction = findViewById(R.id.ll_add_adv_choose_auction);
        llRent = findViewById(R.id.ll_add_adv_choose_rent_car);
        llBuy = findViewById(R.id.ll_add_adv_choose_buy_car);
        clickLusteners();
        brandsModel = (BrandsModel) getIntent().getSerializableExtra("brandsModel");
        if (Tools.getCurrentLanguage(this).equals("ar")){
            back.setRotation(180);
        }
    }

    private void clickLusteners() {

        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!selected.equals("")) {
                    Intent i = new Intent(AddAdvertisingChoose.this, AddAdvertising.class);
                    i.putExtra("selected", selected);
                    i.putExtra("brandsModel", brandsModel);
                    startActivity(i);
                }
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        llBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selected = "buy";
                editState(tvBuy, ivBuy, llBuy, R.mipmap.ic_buy_car,
                        getResources().getDrawable(R.drawable.rounded_blue),
                        getResources().getColor(R.color.blue));


                editState(tvRent, ivRent, llRent, R.mipmap.ic_rent_off,
                        getResources().getDrawable(R.drawable.rounded_grey_without_bg),
                        getResources().getColor(R.color.gray));

                editState(tvAuction, ivAuction, llAuction, R.mipmap.ic_auction_off,
                        getResources().getDrawable(R.drawable.rounded_grey_without_bg),
                        getResources().getColor(R.color.gray));
            }
        });


        llRent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selected = "rent";
                editState(tvRent, ivRent, llRent, R.mipmap.ic_rent_on,
                        getResources().getDrawable(R.drawable.rounded_blue),
                        getResources().getColor(R.color.blue));

                editState(tvBuy, ivBuy, llBuy, R.mipmap.ic_buy_off,
                        getResources().getDrawable(R.drawable.rounded_grey_without_bg),
                        getResources().getColor(R.color.gray));

                editState(tvAuction, ivAuction, llAuction, R.mipmap.ic_auction_off,
                        getResources().getDrawable(R.drawable.rounded_grey_without_bg),
                        getResources().getColor(R.color.gray));
            }
        });


        llAuction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selected = "auction";
                editState(tvAuction, ivAuction, llAuction, R.mipmap.ic_auction_on,
                        getResources().getDrawable(R.drawable.rounded_blue),
                        getResources().getColor(R.color.blue));

                editState(tvRent, ivRent, llRent, R.mipmap.ic_rent_off,
                        getResources().getDrawable(R.drawable.rounded_grey_without_bg),
                        getResources().getColor(R.color.gray));

                editState(tvBuy, ivBuy, llBuy, R.mipmap.ic_buy_off,
                        getResources().getDrawable(R.drawable.rounded_grey_without_bg),
                        getResources().getColor(R.color.gray));
            }
        });
    }

    private void editState(TextView tv, ImageView iv, LinearLayout ll, int Id, Drawable drawable, int Color) {
        tv.setTextColor(Color);
        iv.setImageResource(Id);
        ll.setBackground(drawable);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}

