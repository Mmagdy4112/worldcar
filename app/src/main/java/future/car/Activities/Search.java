package future.car.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.appyvet.materialrangebar.RangeBar;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import future.car.Adapters.FiltersAdapter;
import future.car.Adapters.PopUpAdapter;
import future.car.Adapters.SearchAdapter;
import future.car.Model.BrandsModel;
import future.car.Model.HomeModel;
import future.car.Model.MetaDataModel;
import future.car.Model.SearchModel;
import future.car.R;
import future.car.Utils.NoConnection;
import future.car.Utils.RecyclerViewClickListener;
import future.car.Utils.SessionManager;
import future.car.Utils.Tools;
import future.car.WebServices.Webservice;
import io.michaelrocks.paranoid.Obfuscate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Obfuscate
public class Search extends AppCompatActivity {
    Dialog dialogPopUp;
    AlertDialog.Builder dialogBuilder;
    RecyclerView rcvPopup, rcvData, rcvFilters;
    ImageView back;
    RelativeLayout rltPrice;
    LinearLayout llMAin;
    AppCompatEditText etSearch;
    RangeBar rangeBar;
    AppCompatTextView search, tvPopUpPriceFrom, tvPopUpPriceTo, tvPopupDone, tvPopupName;
    PopUpAdapter popUpAdapter;
    String[] typeArr, yearsArr = new String[52], modelArr, filtersArr, transmissionArr = new String[2],
            speedoMeterArr, cylindersArr, interiorArr, extriorArr;
    String speedChanger = "", type = "", interior = "", extrior = "";
    int model = 0, year = 0, price = 0, speedoMeter = 0, cylinders = 0, priceFrom = 0, priceTo = 0, speedoFrom = 0, speedoTo = 0;
    List<BrandsModel.CarBrands> carBrands;
    SessionManager sessionManager;
    HomeModel searchModels;
    SearchAdapter searchAdapter;
    Dialog dialog;
    FiltersAdapter filtersAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initializeDesign();
    }

    private void fillArrays() {
        carBrands = Home.getBrandsList(this);
        modelArr = new String[carBrands.size()];
        extriorArr = new String[metaDataModel.data.exteriorColors.size()];
        interiorArr = new String[metaDataModel.data.interiorColors.size()];
        cylindersArr = new String[]{"4", "6", "8", "16"};
        typeArr = new String[]{getResources().getString(R.string.buy), getResources().getString(R.string.rent), getResources().getString(R.string.auction)};
        transmissionArr = new String[]{getResources().getString(R.string.automatic), getResources().getString(R.string.manual)};
        speedoMeterArr = new String[16];
        filtersArr = new String[]{
                getResources().getString(R.string.type),
                getResources().getString(R.string.model),
                getResources().getString(R.string.year),
                getResources().getString(R.string.price),
                getResources().getString(R.string.interiorr),
                getResources().getString(R.string.extriorr),
                getResources().getString(R.string.speed_changer),
                getResources().getString(R.string.cylinders2),
                getResources().getString(R.string.speedo_meter)};
        for (int i = 0; i < yearsArr.length; i++) {
            yearsArr[i] = "" + (1970 + i);
        }
        int speed = 100;
        for (int i = 0; i < speedoMeterArr.length; i++) {
            speedoMeterArr[i] = "" + (speed);
            speed += 20;
        }

        for (int i = 0; i < carBrands.size(); i++) {
            modelArr[i] = carBrands.get(i).brandName;
        }


        for (int i = 0; i < metaDataModel.data.exteriorColors.size(); i++) {
            extriorArr[i] = metaDataModel.data.exteriorColors.get(i).color;
        }


        for (int i = 0; i < metaDataModel.data.interiorColors.size(); i++) {
            interiorArr[i] = metaDataModel.data.interiorColors.get(i).color;
        }

        filtersAdapter = new FiltersAdapter(filtersArr, this, new RecyclerViewClickListener() {
            @Override
            public void recyclerViewListClicked(View v2, int position) {

                AppCompatTextView v = ((AppCompatTextView) rcvFilters.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.tv_filters_item));

                if (position == 0) {
                    handlePopUp(1, typeArr, getResources().getString(R.string.type), position);

                } else if (position == 1) {
                    handlePopUp(0, modelArr, getResources().getString(R.string.model), position);

                } else if (position == 2) {
                    handlePopUp(2, yearsArr, getResources().getString(R.string.year), position);

                } else if (position == 3) {
                    handlePopUp(3, null, getResources().getString(R.string.price), position);
                } else if (position == 4) {
                    handlePopUp(4, interiorArr, getResources().getString(R.string.interiorr), position);
                } else if (position == 5) {
                    handlePopUp(5, extriorArr, getResources().getString(R.string.extriorr), position);
                } else if (position == 6) {
                    handlePopUp(6, transmissionArr, getResources().getString(R.string.speed_changer), position);
                } else if (position == 7) {
                    handlePopUp(7, cylindersArr, getResources().getString(R.string.cylinders2), position);
                } else if (position == 8) {
                    handlePopUp(8, speedoMeterArr, getResources().getString(R.string.speedo_meter), position);

                }

            }
        });
        rcvFilters.setAdapter(filtersAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void handlePopUp(final int popUpType, final String arr[], String name, final int rcvPosition) {
        //0 model , 1 type , 2 year , 3 price
        tvPopupName.setText(name);
        if (popUpType == 3) {
            rltPrice.setVisibility(View.VISIBLE);
            rcvPopup.setVisibility(View.GONE);
            tvPopUpPriceFrom.setText("0");
            tvPopUpPriceTo.setText("" + 999 * 10000);
            rangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
                @Override
                public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                    priceFrom = leftPinIndex * 10000;
                    priceTo = rightPinIndex * 10000;
                    tvPopUpPriceFrom.setText("" + leftPinIndex * 10000);
                    tvPopUpPriceTo.setText("" + rightPinIndex * 10000);
                }


            });

            tvPopupDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogPopUp.dismiss();
                }
            });
        } else if (popUpType == 8) {
            rltPrice.setVisibility(View.VISIBLE);
            rcvPopup.setVisibility(View.GONE);
            tvPopUpPriceFrom.setText("0");
            tvPopUpPriceTo.setText("" + 99 * 10000);
            rangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
                @Override
                public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                    speedoFrom = leftPinIndex * 10000;
                    speedoTo = rightPinIndex * 10000;
                    tvPopUpPriceFrom.setText("" + leftPinIndex * 1000);
                    tvPopUpPriceTo.setText("" + rightPinIndex * 1000);
                }


            });

            tvPopupDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogPopUp.dismiss();
                }
            });
        } else {
            rltPrice.setVisibility(View.GONE);
            rcvPopup.setVisibility(View.VISIBLE);
            popUpAdapter = new PopUpAdapter(arr, this, new RecyclerViewClickListener() {
                @Override
                public void recyclerViewListClicked(View v, int position) {
                    if (popUpType == 0) {
                        if (arr != null) {
                            model = carBrands.get(position).brandId;
                        }
                    } else if (popUpType == 1) {
                        if (arr != null) {
                            if (position == 0) {
                                type = "buy";
                            } else if (position == 1) {
                                type = "rent";
                            } else {
                                type = "auction";
                            }

                        }

                    } else if (popUpType == 2) {
                        if (arr != null) {
                            year = Integer.parseInt(arr[position]);

                        }
                    } else if (popUpType == 4) {
                        if (arr != null) {
                            interior = arr[position];

                        }
                    } else if (popUpType == 5) {
                        if (arr != null) {
                            extrior = arr[position];

                        }
                    } else if (popUpType == 6) {
                        if (arr != null) {
                            speedChanger = arr[position];
                        }
                    } else if (popUpType == 7) {
                        if (arr != null) {
                            cylinders = Integer.parseInt(arr[position]);
                        }
                    }

                    filtersArr[rcvPosition] = "" + arr[position];
                    dialogPopUp.dismiss();
                    filtersAdapter.notifyItemChanged(rcvPosition);
                }
            });
            rcvPopup.setAdapter(popUpAdapter);
        }

        dialogPopUp.show();

    }

    public void popUp() {
        View view = LayoutInflater.from(this).inflate(
                R.layout.pop_up, null);

        rltPrice = view.findViewById(R.id.rlt_ppopup_price);
        rcvPopup = view.findViewById(R.id.rcv_popup);
        rangeBar = view.findViewById(R.id.rangebar_popup);
        tvPopupDone = view.findViewById(R.id.tv_popup_done);
        tvPopUpPriceFrom = view.findViewById(R.id.tv_popup_from);
        tvPopupName = view.findViewById(R.id.tv_popup_name);
        tvPopUpPriceTo = view.findViewById(R.id.tv_popup_to);
        rcvPopup.setLayoutManager(new LinearLayoutManager(this));
        dialogBuilder = new AlertDialog.Builder(this, R.style.CustomDialog);
        dialogBuilder.setCancelable(true);
        dialogBuilder.setView(view);
        dialogPopUp = new Dialog(this);
        dialogPopUp = dialogBuilder.create();

    }

    private void initializeDesign() {
        sessionManager = new SessionManager(this);
        llMAin = findViewById(R.id.ll_search);
        search = findViewById(R.id.tv_search);
        rcvData = findViewById(R.id.rcv_search);
        rcvFilters = findViewById(R.id.rcv_search_filters);
        etSearch = findViewById(R.id.et_search);
        back = findViewById(R.id.image_search_back);
        rcvData.setLayoutManager(new GridLayoutManager(this, 3));
        rcvFilters.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        getMetaData();
        fillArrays();
        popUp();
        clickLusteners();
        if (Tools.getCurrentLanguage(this).equals("ar")) {
            back.setRotation(180);
        }
    }

    private void clickLusteners() {
        //0 model , 1 type , 2 year , 3 price

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }


    private SearchModel searchModel() {
        final SearchModel searchModel = new SearchModel();
        searchModel.name = etSearch.getText().toString();
        if (!type.equals("")) {
            searchModel.type = type;
        }
        if (model!=0) {
            searchModel.brandId = model;
        }
        if (year!=0) {
            searchModel.year = year;
        }
        if (priceTo!=0) {
            searchModel.priceTo = priceTo;
        }
        if (priceFrom != 0) {
            searchModel.priceFrom = priceFrom;
        }
        if (!interior.equals("")) {
            searchModel.interiorColor = interior;
        }
        if (!extrior.equals("")) {
            searchModel.exteriorColor = extrior;
        }
        if (!speedChanger.equals("")) {
            searchModel.gearbox = speedChanger;
        }
        if (cylinders!=0) {
            searchModel.cylindersNumber = cylinders;
        }
        if (speedoTo != 0) {
            searchModel.mileage = speedoTo;
        }



        return searchModel;
    }

    private void search() {
        dialog = Tools.createLoadingBar(this);
        dialog.show();


        String auth = "Bearer " + sessionManager.getToken();
        Map<String, String> params = new HashMap<>();
        params.put("Content-Type", "application/json");
        if (sessionManager.isLoggedIn()) {
            params.put("Authorization", auth);
        }
        params.put("Content-Language", Tools.getCurrentLanguage(this));
        params.put("Accept", "application/json");
        Webservice.getInstance().getApi().seatch(params, searchModel()).enqueue(new Callback<HomeModel>() {
            @Override
            public void onResponse(Call<HomeModel> call, Response<HomeModel> response) {
                if (response.body() != null) {
                    if (response.body().status == 200) {
                        searchModels = response.body();
                        if (searchModels != null) {
                            searchAdapter = new SearchAdapter(searchModels.cars, Search.this, new RecyclerViewClickListener() {
                                @Override
                                public void recyclerViewListClicked(View v, int position) {
                                    final Intent i = new Intent(Search.this, Car.class);
                                    i.putExtra("carDetails", searchModels.cars.get(position));
                                    startActivity(i);

                                }
                            });

                            rcvData.setAdapter(searchAdapter);
                        } else {
                            rcvData.setAdapter(null);
                            Toast.makeText(Search.this, "" + getResources().getString(R.string.no_results), Toast.LENGTH_SHORT).show();
                        }

                    }
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<HomeModel> call, Throwable t) {
                dialog.dismiss();
                Tools.noConnection(Search.this, llMAin, new NoConnection() {
                    @Override
                    public void passedMethod() {
                        search();
                    }
                });
            }
        });
    }

    MetaDataModel metaDataModel;

    private void getMetaData() {
        Type type = new TypeToken<MetaDataModel>() {
        }.getType();
        metaDataModel = (MetaDataModel) Tools.getObjList(this, "MetaDataModel", type);
    }
}
