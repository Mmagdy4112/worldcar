package future.car.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import future.car.Adapters.CarImagesAdapter;
import future.car.Adapters.CarSaleHomeAdapter;
import future.car.Model.CarBidModel;
import future.car.Model.HomeModel;
import future.car.Model.MainResponse;
import future.car.R;
import future.car.Utils.NoConnection;
import future.car.Utils.RecyclerViewClickListener;
import future.car.Utils.SessionManager;
import future.car.Utils.Tools;
import future.car.WebServices.Webservice;
import io.michaelrocks.paranoid.Obfuscate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Obfuscate
public class Car extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {
    HomeModel.CarDetails carDetails;
    TextView tvName, tvPrice, tvModel, tvYear, tvExtrior, tvintrior, tvPayment, tvSpeedCharger, tvClyinders, tvBedding, tvSunroof, tvCamera,
            tvSensores, tvBluetooth, tvGps, tvCurrentPrice, tvMinimumBid, tvCarEndTime, tvCarTimeLeft, tvNumberBidding, tvDescription;
    LinearLayout llBidContainer, llAuctionButton, llPrice;
    ScrollView scrollView;
    ImageView back;
    Button contactSeller;
    SliderLayout sliderLayout;
    RecyclerView rcvCars, rcvImages;
    LinearLayout llNonAuction, mainLayout;
    AppCompatButton btnBiding;
    DefaultSliderView defaultSliderView;
    CarImagesAdapter carImagesAdapter;
    String type = "";
    boolean bidEnd = false, carOwner = false;
    Dialog dialogLogin, dialogLoading;
    SessionManager sessionManager;
    HomeModel homeModel;
    CarSaleHomeAdapter carSaleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car);
        initializeDesign();


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        sliderLayout.stopAutoCycle();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initializeDesign() {
        sessionManager = new SessionManager(this);
        dialogLogin = Tools.loginPopUp(this);
        dialogLoading = Tools.createLoadingBar(this);
        contactSeller = findViewById(R.id.btn_car_contact_seller);
        back = findViewById(R.id.image_car_back);
        llPrice = findViewById(R.id.ll_price);
        tvName = findViewById(R.id.tv_car_name);
        tvPrice = findViewById(R.id.tv_car_price);
        tvModel = findViewById(R.id.tv_car_model);
        tvYear = findViewById(R.id.tv_car_year);
        tvExtrior = findViewById(R.id.tv_car_exterior);
        tvintrior = findViewById(R.id.tv_car_interior);
        tvPayment = findViewById(R.id.tv_car_payment);
        scrollView = findViewById(R.id.scroll);
        tvSpeedCharger = findViewById(R.id.tv_car_type_of_speed);
        tvClyinders = findViewById(R.id.tv_car_cylinders);
        tvBedding = findViewById(R.id.tv_car_bedding);
        tvSunroof = findViewById(R.id.tv_car_sunroof);
        tvCamera = findViewById(R.id.tv_car_camera);
        tvSensores = findViewById(R.id.tv_car_sensors);
        tvDescription = findViewById(R.id.tv_car_details);
        tvBluetooth = findViewById(R.id.tv_car_bluetooth);
        tvGps = findViewById(R.id.tv_car_gps);
        sliderLayout = findViewById(R.id.slider_cars);
        btnBiding = findViewById(R.id.btn_car_bidding);
        rcvCars = findViewById(R.id.rcv_car);
        llNonAuction = findViewById(R.id.ll_car_non_auction);
        rcvImages = findViewById(R.id.rcv_car_images);
        mainLayout = findViewById(R.id.ll_car);
        llBidContainer = findViewById(R.id.ll_car_bid);
        llAuctionButton = findViewById(R.id.ll_car_auction);
        tvCurrentPrice = findViewById(R.id.current_price);
        tvMinimumBid = findViewById(R.id.tv_car_minimum_bid);
        tvCarEndTime = findViewById(R.id.tv_car_end_time);
        tvCarTimeLeft = findViewById(R.id.tv_car_time_left);
        tvNumberBidding = findViewById(R.id.tv_car_number_of_biding);
        rcvImages.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        clickLusteners();
        setData();


        if (Tools.getCurrentLanguage(this).equals("ar")) {
            back.setRotation(180);
            tvPrice.setTextDirection(View.TEXT_DIRECTION_RTL);
            tvModel.setTextDirection(View.TEXT_DIRECTION_RTL);
            tvYear.setTextDirection(View.TEXT_DIRECTION_RTL);
            tvExtrior.setTextDirection(View.TEXT_DIRECTION_RTL);
            tvintrior.setTextDirection(View.TEXT_DIRECTION_RTL);
            tvPayment.setTextDirection(View.TEXT_DIRECTION_RTL);
            tvSpeedCharger.setTextDirection(View.TEXT_DIRECTION_RTL);
            tvClyinders.setTextDirection(View.TEXT_DIRECTION_RTL);
            tvBedding.setTextDirection(View.TEXT_DIRECTION_RTL);
            tvSunroof.setTextDirection(View.TEXT_DIRECTION_RTL);
            tvCamera.setTextDirection(View.TEXT_DIRECTION_RTL);
            tvSensores.setTextDirection(View.TEXT_DIRECTION_RTL);
            tvDescription.setTextDirection(View.TEXT_DIRECTION_RTL);
            tvBluetooth.setTextDirection(View.TEXT_DIRECTION_RTL);
            tvGps.setTextDirection(View.TEXT_DIRECTION_RTL);
            tvCarEndTime.setTextDirection(View.TEXT_DIRECTION_RTL);
            tvCarTimeLeft.setTextDirection(View.TEXT_DIRECTION_RTL);
            tvNumberBidding.setTextDirection(View.TEXT_DIRECTION_RTL);
        }
    }

    private void clickLusteners() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        contactSeller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sessionManager.isLoggedIn()) {
                    Intent i = new Intent(Car.this, ContactSeller.class);
                    i.putExtra("userEmail", carDetails.userEmail);
                    i.putExtra("userMobile", carDetails.userMobile);
                    i.putExtra("userName", carDetails.userName);
                    i.putExtra("userId", carDetails.userId);
                    startActivity(i);
                } else {
                    dialogLogin.show();
                }

            }
        });
    }

    private void setData() {
        carDetails = (HomeModel.CarDetails) getIntent().getSerializableExtra("carDetails");
        type = carDetails.saleType;
        tvName.setText(carDetails.title);
        tvPrice.setText(carDetails.price);
        tvModel.setText(carDetails.carBrand);
        tvYear.setText(carDetails.year);
        tvExtrior.setText(carDetails.exteriorColor);
        tvintrior.setText(carDetails.interiorColor);
        tvPayment.setText(carDetails.saleType);
        tvSpeedCharger.setText(carDetails.gearbox);
        tvClyinders.setText(carDetails.cylindersNumber);
        tvBedding.setText(carDetails.beddingType);
        tvDescription.setText(carDetails.notes);


        setSwitchers(tvSunroof,carDetails.sunroof);
        setSwitchers(tvCamera,carDetails.camera);
        setSwitchers(tvSensores,carDetails.sensors);
        setSwitchers(tvBluetooth,carDetails.bluetooth);
        setSwitchers(tvGps,carDetails.mapSystem);

        if (type.equals("auction")) {
            showBid();
            bidingPopUp(this);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) scrollView
                    .getLayoutParams();
            int dpValue = 80; // margin in dips
            float d = getResources().getDisplayMetrics().density;
            int margin = (int) (dpValue * d);
            layoutParams.setMargins(0, 0, 0, margin);
            scrollView.setLayoutParams(layoutParams);
            llNonAuction.setVisibility(View.GONE);
            llAuctionButton.setVisibility(View.VISIBLE);
            llBidContainer.setVisibility(View.VISIBLE);
            tvMinimumBid.setText(carDetails.minimumBid);
            tvCurrentPrice.setText(carDetails.startBid);
            tvCarTimeLeft.setText(carDetails.bidTimeLeft);
            tvNumberBidding.setText(carDetails.numberOfBidding);
            tvCarEndTime.setText(carDetails.bidEndTime);
            llPrice.setVisibility(View.GONE);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date start = null, end = null;
            try {
                start = sdf.parse(sdf.format(Calendar.getInstance().getTime()));
                end = sdf.parse(carDetails.bidEndTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (start.after(end)) {
                bidEnd = true;
            }
            if (sessionManager.getId().equals(String.valueOf(carDetails.userId))) {
                carOwner = true;
            }

            Log.d("setData: ", sessionManager.getId() + " " + carDetails.userId);

            btnBiding.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (sessionManager.isLoggedIn()) {

                        if (!carOwner) {
                            if (!bidEnd) {
                                bidingDialog.show();
                            } else {
                                Toast.makeText(Car.this, "" + getResources().getString(R.string.auction_end), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }else {
                        dialogLogin.show();
                    }

                }
            });
        } else {
            getData();
        }
        slider();
    }

    private void setSwitchers(TextView tvName,String type){
        if (type.equals("yes")){
            tvName.setText(getResources().getString(R.string.yes));
        }else{
            tvName.setText(getResources().getString(R.string.no));
        }
    }
    Dialog bidingDialog;

    private void bidingPopUp(final Context context) {
        AppCompatButton btnBid;
        final AppCompatEditText etBid;
        AppCompatTextView tvBid;
        View view = LayoutInflater.from(context).inflate(
                R.layout.biding_popup, null);
        bidingDialog = new Dialog(context);
        bidingDialog.setCancelable(true);
        bidingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        bidingDialog.setContentView(view);
        bidingDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        btnBid = view.findViewById(R.id.btn_biding_popup);
        etBid = view.findViewById(R.id.et_biding_popup_price);
        tvBid = view.findViewById(R.id.tv_biding_popup_price);
        tvBid.setText(getResources().getString(R.string.current_price) + " : " + tvCurrentPrice.getText().toString());

        etBid.setText(tvCurrentPrice.getText().toString());
        btnBid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((etBid.getText().toString().isEmpty())) {
                    etBid.setText("0");
                }
                if (Integer.parseInt(etBid.getText().toString()) < Integer.parseInt(tvCurrentPrice.getText().toString())) {
                    Toast.makeText(Car.this, "" + getResources().getString(R.string.cannot_bid), Toast.LENGTH_LONG).show();
                } else {
                    bidOnCar(etBid.getText().toString());
                }

                bidingDialog.dismiss();
            }
        });

    }

    private void bidOnCar(final String price) {
        dialogLoading.show();
        String auth = "Bearer " + sessionManager.getToken();
        Map<String, String> params = new HashMap<>();
        params.put("Content-Type", "application/json");
        params.put("Authorization", auth);
        params.put("Accept", "application/json");

        CarBidModel carBidModel = new CarBidModel();
        carBidModel.car_id = carDetails.carId;
        carBidModel.price = Integer.valueOf(price);

        Webservice.getInstance().getApi().bidOnCar(params, carBidModel).enqueue(new Callback<MainResponse>() {
            @Override
            public void onResponse(Call<MainResponse> call, Response<MainResponse> response) {
                if (response.body() != null) {
                    if (response.body().status == 200) {
                        showBid();
                        Toast.makeText(Car.this, "" + getResources().getString(R.string.bid_added), Toast.LENGTH_SHORT).show();
                    }
                }

                dialogLoading.dismiss();
            }

            @Override
            public void onFailure(Call<MainResponse> call, Throwable t) {
                Log.d("onFailure: ", t.getMessage());
                dialogLoading.dismiss();
                Tools.noConnection(Car.this, mainLayout, new NoConnection() {
                    @Override
                    public void passedMethod() {
                        bidOnCar(price);
                    }
                });
            }
        });
    }

    int lastPosition = 0;

    private void slider() {

        carImagesAdapter = new CarImagesAdapter(carDetails.carImages, this, new RecyclerViewClickListener() {
            @Override
            public void recyclerViewListClicked(View v, int position) {
                if (position > lastPosition) {

                    for (int i = lastPosition; i <= position; i++) {
                        sliderLayout.setCurrentPosition(i);
                    }
                } else {
                    for (int i = lastPosition; i >= position; i--) {
                        sliderLayout.setCurrentPosition(i);
                    }
                }
                lastPosition = position;

            }
        });
        rcvImages.setAdapter(carImagesAdapter);
        try {
            for (int i = 0; i < carDetails.carImages.size(); i++) {
                String image = carDetails.carImages.get(i);
                // Toast.makeText(getApplicationContext(),""+image,Toast.LENGTH_LONG).show();

                try {


                    defaultSliderView = new DefaultSliderView(this);
                    defaultSliderView
                            .image(image)
                            .setScaleType(BaseSliderView.ScaleType.Fit)
                            .setOnSliderClickListener(Car.this);
                    defaultSliderView.bundle(new Bundle());
//                    defaultSliderView.getBundle()
//                            .putString("extra", title);
                    defaultSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            Intent intent = new Intent(Car.this, ZoomedImage.class);
                            intent.putExtra("image", slider.getUrl());
                            startActivity(intent);
                        }
                    });
                    sliderLayout.addSlider(defaultSliderView);
                    sliderLayout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
                } catch (Exception e) {

                }

            }
            sliderLayout.setPresetTransformer(SliderLayout.Transformer.ZoomOut);
            sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
            sliderLayout.setCustomAnimation(new DescriptionAnimation());
            sliderLayout.setDuration(4000);
            sliderLayout.getPagerIndicator().setDefaultIndicatorColor(getResources().getColor(R.color.colorAccent), getResources().getColor(R.color.gray));
            sliderLayout.addOnPageChangeListener(Car.this);
            sliderLayout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
        } catch (Exception e) {
            e.getStackTrace();
        }

    }


    public void getData() {
        dialogLoading.show();
        final String lang = Tools.getCurrentLanguage(this);
        Map<String, String> params = new HashMap<>();
        params.put("Content-Language", lang);

        Webservice.getInstance().getApi().getBuyCars(params).enqueue(new Callback<HomeModel>() {
            @Override
            public void onResponse(Call<HomeModel> call, Response<HomeModel> response) {
                if (response.body() != null) {
                    if (response.body().status == 200) {
                        homeModel = response.body();
                        carSaleAdapter = new CarSaleHomeAdapter(homeModel.carBuy, Car.this, new RecyclerViewClickListener() {
                            @Override
                            public void recyclerViewListClicked(View v, int position) {
                                Intent i = new Intent(Car.this, Car.class);
                                i.putExtra("carDetails", homeModel.carBuy.get(position));
                                i.putExtra("selected", 0);
                                finish();
                                startActivity(i);
                            }
                        });
                        rcvCars.setLayoutManager(new LinearLayoutManager(Car.this, LinearLayoutManager.HORIZONTAL, false));
                        rcvCars.setAdapter(carSaleAdapter);
                    }

                }
                dialogLoading.dismiss();
            }

            @Override
            public void onFailure(Call<HomeModel> call, Throwable t) {
                dialogLoading.dismiss();
                Tools.noConnection(Car.this, mainLayout, new future.car.Utils.NoConnection() {
                    @Override
                    public void passedMethod() {
                        getData();
                    }
                });

            }
        });
    }

    List<CarBidModel.Bid> carBids;

    private void showBid() {
        String auth = "Bearer " + sessionManager.getToken();
        Map<String, String> params = new HashMap<>();
        params.put("Content-Type", "application/json");
        params.put("Authorization", auth);
        params.put("Accept", "application/json");

        CarBidModel carBidModel = new CarBidModel();
        carBidModel.car_id = carDetails.carId;

        Webservice.getInstance().getApi().showBid(params, carBidModel).enqueue(new Callback<CarBidModel>() {
            @Override
            public void onResponse(Call<CarBidModel> call, final Response<CarBidModel> response) {
                if (response.body() != null) {
                    if (response.body().status == 200) {
                        if (response.body().carBid.bids.size() >= 1) {
                            carBids = response.body().carBid.bids;
                            tvCurrentPrice.setText("" + response.body().carBid.highPrice);
                            tvCurrentPrice.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent i = new Intent(Car.this, BidHistory.class);
                                    i.putExtra("carBids", (Serializable) carBids);
                                    i.putExtra("bidEndTime", carDetails.bidEndTime);
                                    i.putExtra("bidTimeLeft", carDetails.bidTimeLeft);
                                    i.putExtra("bidders", response.body().carBid.bidders);
                                    i.putExtra("carOwner", carOwner);
                                    i.putExtra("bidEnd", bidEnd);
                                    startActivity(i);
                                }
                            });

                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<CarBidModel> call, Throwable t) {

            }
        });
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        lastPosition = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
