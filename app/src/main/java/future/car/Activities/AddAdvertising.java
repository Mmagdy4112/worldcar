package future.car.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import future.car.ImageHandle.CompressObject;
import future.car.ImageHandle.FileOperations;
import future.car.ImageHandle.ImageCompression;
import future.car.Model.AddCarModel;
import future.car.Model.BrandsModel;
import future.car.Model.CountriesModel;
import future.car.Model.MainResponse;
import future.car.Model.MetaDataModel;
import future.car.R;
import future.car.Utils.NoConnection;
import future.car.Utils.SessionManager;
import future.car.Utils.Tools;
import future.car.WebServices.Webservice;
import io.michaelrocks.paranoid.Obfuscate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
@Obfuscate
public class AddAdvertising extends AppCompatActivity {
    public final static int IMAGE_CODE = 10, ACTION_GET_CONTENT = 1265, PICK_FROM_CAMERA = 1280;
    ImageView back, imageOne, imageTwo, imageThree, imageFour, imageFive, imageSix, imageSeven, imageEight;
    LinearLayout llModel, llBedding, llCategory, llBid, llCountry, llTransmission, llAuction, mainLyout, llBidImages, llNonAuction, llExtrior, llIntrior, llClyinders;
    AppCompatTextView tvModel, tvBedding, tvCategory, tvBid, tvCountry, tvTransmission, tvExtrior, tvInterior, tvCylinders;
    Spinner spModel, spBedding, spCategory, spBid, spCountry, spTransmission, spExtrior, spInterior, spCylinders;
    AppCompatEditText etTitle, etStartBid, etMiniBid, etPhone, etPhoneAuction, etDescription, etPrice, etYear, etKilometers;
    Switch swSunroof, swCamer, swSenseors, swBluetooth, swMap;
    AppCompatButton publish;
    ImageView imageViews[];
    String[] imagesArr = new String[8], transmissionArr = new String[2], countriesArr, modelsArr, typesArr, bidTimeArr, specificationArr = new String[5], cylindersArr, interiorArr, extriorArr, beddingArr;
    int model = -1, category = -1, country = -1, imageIndex = 0, modelPostion = 0, years = 0, kilometers = 0,extriorSt = 0, interiorSt = 0,beddingType=0;
    String titleSt = "", startBidSt = "", minimumBidSt = "", bidTimeSt = "",
            phoneSt = "",  descriptionSt = "",
            registerType = "", transmision = "", price = "",   cylinders = "";
    List<CountriesModel.Country> countries;
    BrandsModel brandsModel;
    SessionManager sessionManager;
    Dialog dialog;
    MetaDataModel metaDataModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_advertising);
        initializeDesign();
        getCountries();
    }

    private String convertToBase64(Uri path, ImageView imageView) {
        FileOperations fileOperations = new FileOperations();
        String imagePath = fileOperations.getPath(this, path);
        File file = new File(imagePath);
        CompressObject compressObject = new ImageCompression(this).compressImage(imagePath, file.getName());
        imageView.setImageBitmap(compressObject.getImage());
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setBackground(getResources().getDrawable(R.color.transparent));
        Log.d("convertToBase64", "" + fileOperations.BitMapToString(compressObject.getImage()));
        return fileOperations.BitMapToString(compressObject.getImage());

    }

    public void selectImage() {
        Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(galleryIntent, ACTION_GET_CONTENT);
    }

    private void initializeDesign() {
        sessionManager = new SessionManager(this);
        mainLyout = findViewById(R.id.ll_add_adv);
        back = findViewById(R.id.image_add_adv_back);
        imageOne = findViewById(R.id.image_add_adv_one);
        imageTwo = findViewById(R.id.image_add_adv_two);
        imageThree = findViewById(R.id.image_add_adv_three);
        imageFour = findViewById(R.id.image_add_adv_four);
        imageFive = findViewById(R.id.image_add_adv_five);
        imageSix = findViewById(R.id.image_add_adv_six);
        imageSeven = findViewById(R.id.image_add_adv_seven);
        imageEight = findViewById(R.id.image_add_adv_eight);
        swSunroof = findViewById(R.id.switch_add_adv_sunroof);
        swCamer = findViewById(R.id.switch_add_adv_scamera);
        swSenseors = findViewById(R.id.switch_add_adv_sensors);
        swBluetooth = findViewById(R.id.switch_add_adv_bluetooth);
        swMap = findViewById(R.id.switch_add_adv_map);
        etTitle = findViewById(R.id.et_add_adv_title);
        etStartBid = findViewById(R.id.et_add_adv_start_bid);
        etMiniBid = findViewById(R.id.et_add_adv_minimum_bid);
        etPhoneAuction = findViewById(R.id.et_add_adv_auction_phone);
        etPhone = findViewById(R.id.et_add_adv_bid_phone);
        llExtrior = findViewById(R.id.ll_add_adv_extrior);
        tvExtrior = findViewById(R.id.tv_add_adv_extrior);
        spExtrior = findViewById(R.id.sp_add_adv_extrior);
        llIntrior = findViewById(R.id.ll_add_adv_interior);
        tvInterior = findViewById(R.id.tv_add_adv_interior);
        spInterior = findViewById(R.id.sp_add_adv_interior);
        etPrice = findViewById(R.id.et_add_adv_price);
        llClyinders = findViewById(R.id.ll_add_adv_cylinders);
        tvCylinders = findViewById(R.id.tv_add_adv_cylinders);
        spCylinders = findViewById(R.id.sp_add_adv_cylinders);
        etYear = findViewById(R.id.et_add_adv_year);
        etKilometers = findViewById(R.id.et_add_adv_kilometers);
        etDescription = findViewById(R.id.et_add_adv_description);
        llModel = findViewById(R.id.ll_add_adv_model);
        llBedding = findViewById(R.id.ll_add_adv_bedding);
        llCategory = findViewById(R.id.ll_add_adv_category);
        llBid = findViewById(R.id.ll_add_adv_bid_time);
        llCountry = findViewById(R.id.ll_add_adv_country);
        llBidImages = findViewById(R.id.ll_add_adv_bid_images);
        llTransmission = findViewById(R.id.ll_add_adv_transmission);
        llAuction = findViewById(R.id.ll_add_adv_auction);
        llNonAuction = findViewById(R.id.ll_non_auction);
        tvBedding = findViewById(R.id.tv_add_adv_bedding);
        tvModel = findViewById(R.id.tv_add_adv_model);
        tvCategory = findViewById(R.id.tv_add_adv_category);
        tvBid = findViewById(R.id.tv_add_adv_bid_time);
        tvCountry = findViewById(R.id.tv_add_adv_country);
        tvTransmission = findViewById(R.id.tv_add_adv_transmission);
        spBedding = findViewById(R.id.sp_add_adv_bedding);
        spModel = findViewById(R.id.sp_add_adv_model);
        spCategory = findViewById(R.id.sp_add_adv_category);
        spBid = findViewById(R.id.sp_add_adv_bid_time);
        spCountry = findViewById(R.id.sp_add_adv_country);
        spTransmission = findViewById(R.id.sp_add_adv_transmission);
        publish = findViewById(R.id.btn_add_adv_publish);

        getMetaData();
        setArrays();
        clickListeners();
        if (Tools.getCurrentLanguage(this).equals("ar")) {
            back.setRotation(180);
        }
    }

    private void setArrays() {
        brandsModel = (BrandsModel) getIntent().getSerializableExtra("brandsModel");
        registerType = getIntent().getStringExtra("selected");
        modelsArr = new String[brandsModel.carBrands.size()];
        cylindersArr = new String[]{"4", "6", "8", "16"};
        extriorArr = new String[metaDataModel.data.exteriorColors.size()];
        interiorArr = new String[metaDataModel.data.interiorColors.size()];
        beddingArr = new String[metaDataModel.data.beddingTypes.size()];
        bidTimeArr = new String[6];
        for (int i = 0; i < specificationArr.length; i++) {
            specificationArr[i] = "no";
        }
        for (int i = 0; i < brandsModel.carBrands.size(); i++) {
            modelsArr[i] = brandsModel.carBrands.get(i).brandName;

        }

        for (int i = 0; i < bidTimeArr.length; i++) {
            bidTimeArr[i] = "" + (i + 5) + " " + getResources().getString(R.string.day);
        }


        for (int i = 0; i < metaDataModel.data.exteriorColors.size(); i++) {
            extriorArr[i] = metaDataModel.data.exteriorColors.get(i).color;
        }
        for (int i = 0; i < metaDataModel.data.beddingTypes.size(); i++) {
            beddingArr[i] = metaDataModel.data.beddingTypes.get(i).name;
        }


        for (int i = 0; i < metaDataModel.data.interiorColors.size(); i++) {
            interiorArr[i] = metaDataModel.data.interiorColors.get(i).color;
        }
        transmissionArr[0] = getResources().getString(R.string.automatic);
        transmissionArr[1] = getResources().getString(R.string.manual);

        showSpinner(spCategory, tvCategory, 3, modelsArr);
        showSpinner(spTransmission, tvTransmission, 0, transmissionArr);
        showSpinner(spBid, tvBid, 4, bidTimeArr);
        showSpinner(spExtrior, tvExtrior, 5, extriorArr);
        showSpinner(spInterior, tvInterior, 6, interiorArr);
        showSpinner(spCylinders, tvCylinders, 7, cylindersArr);
        showSpinner(spBedding, tvBedding, 8, beddingArr);


        if (registerType.equals("auction")) {
            llAuction.setVisibility(View.VISIBLE);
            llBidImages.setVisibility(View.VISIBLE);
            llNonAuction.setVisibility(View.GONE);
            imageViews = new ImageView[]{imageOne, imageTwo, imageThree, imageFour, imageFive, imageSix, imageSeven, imageEight};
        } else {
            llBidImages.setVisibility(View.GONE);
            imageViews = new ImageView[]{imageOne, imageTwo, imageThree, imageFour, imageFive, imageSix};
        }

    }


    private void getMetaData() {
        Type type = new TypeToken<MetaDataModel>() {
        }.getType();
        metaDataModel = (MetaDataModel) Tools.getObjList(this, "MetaDataModel", type);
    }

    private void clickListeners() {
        swSunroof.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    specificationArr[0] = "yes";
                } else {
                    specificationArr[0] = "no";
                }
            }
        });

        swCamer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    specificationArr[1] = "yes";
                } else {
                    specificationArr[1] = "no";
                }
            }
        });


        swSenseors.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    specificationArr[2] = "yes";
                } else {
                    specificationArr[2] = "no";
                }
            }
        });


        swBluetooth.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    specificationArr[3] = "yes";
                } else {
                    specificationArr[3] = "no";
                }
            }
        });


        swMap.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    specificationArr[4] = "yes";
                } else {
                    specificationArr[4] = "no";
                }
            }
        });

        publish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                send();
            }
        });

        imageOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
                imageIndex = 0;
            }
        });
        imageTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
                imageIndex = 1;
            }
        });
        imageThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
                imageIndex = 2;
            }
        });
        imageFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
                imageIndex = 3;
            }
        });
        imageFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
                imageIndex = 4;
            }
        });
        imageSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
                imageIndex = 5;
            }
        });
        imageSeven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
                imageIndex = 6;
            }
        });
        imageEight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
                imageIndex = 7;
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void send() {
        intialize();

        if (!validate()) {
            Toast.makeText(this, getResources().getString(R.string.registration_failed), Toast.LENGTH_SHORT).show();

        } else {
            sendData();
        }
    }

    public boolean validate() {
        boolean vaild = true;


        if (titleSt.isEmpty()) {
            vaild = false;
            etTitle.setError(getResources().getString(R.string.invaild_input));
        }

        if (registerType.equals("auction")) {
            if (startBidSt.isEmpty()) {
                vaild = false;
                etStartBid.setError(getResources().getString(R.string.invaild_input));
            }


            if (minimumBidSt.isEmpty()) {
                vaild = false;
                etMiniBid.setError(getResources().getString(R.string.invaild_input));
            }

            if (phoneSt.isEmpty()) {
                vaild = false;
                etPhoneAuction.setError(getResources().getString(R.string.invaild_input));
            }


            if (!startBidSt.isEmpty() && !minimumBidSt.isEmpty()) {
                if (Integer.parseInt(startBidSt) < Integer.parseInt(minimumBidSt)) {
                    startBidSt = minimumBidSt;
                    etStartBid.setText(startBidSt);
                }
            }
        } else {

            if (price.isEmpty()) {
                vaild = false;
                etPrice.setError(getResources().getString(R.string.invaild_input));
            }
            if (phoneSt.isEmpty()) {
                vaild = false;
                etPhone.setError(getResources().getString(R.string.invaild_input));
            }

        }

//
//        if (interiorSt.equals(getResources().getString(R.string.interiorr))) {
//            vaild = false;
//            tvInterior.setError(getResources().getString(R.string.invaild_input));
//        } else {
//            tvInterior.setError(null);
//        }
//
//
//        if (extriorSt.equals(getResources().getString(R.string.extriorr))) {
//            vaild = false;
//            tvExtrior.setError(getResources().getString(R.string.invaild_input));
//        } else {
//            tvExtrior.setError(null);
//        }


        if (descriptionSt.isEmpty()) {
            vaild = false;
            etDescription.setError(getResources().getString(R.string.invaild_input));
        } else {
            etDescription.setError(null);
        }


        if (etKilometers.getText().toString().isEmpty()) {
            vaild = false;
            etKilometers.setError(getResources().getString(R.string.invaild_input));
        } else {
            etKilometers.setError(null);
        }


        if (etYear.getText().toString().isEmpty()) {
            vaild = false;
            etYear.setError(getResources().getString(R.string.invaild_input));
        } else {
            etYear.setError(null);
        }


        if (tvCylinders.getText().toString().isEmpty() || tvCylinders.getText().toString().equals(getResources().getString(R.string.cylinders2))) {
            vaild = false;
            tvCylinders.setError(getResources().getString(R.string.invaild_input));
        } else {
            tvCylinders.setError(null);
        }


        return vaild;
    }

    public void intialize() {

        descriptionSt = etDescription.getText().toString();
        minimumBidSt = etMiniBid.getText().toString();
        startBidSt = etStartBid.getText().toString();
        titleSt = etTitle.getText().toString();
        price = etPrice.getText().toString();
        if (!etYear.getText().toString().equals("")) {
            years = Integer.parseInt(etYear.getText().toString());
        }
        if (!etKilometers.getText().toString().equals("")) {
            kilometers = Integer.parseInt(etKilometers.getText().toString());
        }
        if (!tvCylinders.getText().toString().equals("")) {
            cylinders = tvCylinders.getText().toString();
        }

        if (price.equals("")) {
            price = "0";
        }

        if (registerType.equals("auction")) {
            phoneSt = etPhoneAuction.getText().toString();
        } else {
            phoneSt = etPhone.getText().toString();
        }


    }

    private void sendData() {
        dialog = Tools.createLoadingBar(this);
        dialog.show();

        AddCarModel addCarModel = new AddCarModel();
        addCarModel.saleType = registerType;
        addCarModel.brandId = category;
        addCarModel.typeId = model;
        addCarModel.title = titleSt;
        addCarModel.price = price;
        addCarModel.phone = phoneSt;
        addCarModel.countryId = country;
        addCarModel.exteriorColor = extriorSt;
        addCarModel.interiorColor = interiorSt;
        addCarModel.sunroof = specificationArr[0];
        addCarModel.camera = specificationArr[1];
        addCarModel.sensors = specificationArr[2];
        addCarModel.bluetooth = specificationArr[3];
        addCarModel.mapSystem = specificationArr[4];
        addCarModel.notes = descriptionSt;
        addCarModel.gearbox = transmision;
        addCarModel.carImages = fillterArray(imagesArr);
        Log.d( "sendData: ",""+fillterArray(imagesArr).length);
        addCarModel.cityId = 1;
        addCarModel.year = years;
        addCarModel.cylindersNumber = Integer.valueOf(cylinders);
        addCarModel.beddingType = beddingType;
        addCarModel.mileage = kilometers;
        if (registerType.equals("auction")) {
            String arr[] = bidTimeSt.split(" ");
            int bid = Integer.parseInt(arr[0]);
            addCarModel.startBid = startBidSt;
            addCarModel.bidTime = Integer.valueOf(bid);
            addCarModel.minimumBid = minimumBidSt;
        }
        final String lang = Tools.getCurrentLanguage(this);
        String auth = "Bearer " + sessionManager.getToken();
        Map<String, String> params = new HashMap<>();

        params.put("Content-Type", "application/json");
        params.put("Authorization", auth);
        params.put("Accept", "application/json");
        params.put("Accept-Language", lang);
        Log.d("response", "" + auth);
//        params.put("Authorization", "Bearer "+sessionManager.getToken());
        Webservice.getInstance().getApi().addCar(params, addCarModel).enqueue(new Callback<MainResponse>() {
            @Override
            public void onResponse(Call<MainResponse> call, Response<MainResponse> response) {
                Log.d("response", "" + response.body());
                if (response.body() != null) {
                    if (response.body().status == 200) {
                        Intent i = new Intent(AddAdvertising.this,Home.class);
                        finishAffinity();
                        startActivity(i);
                    }
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<MainResponse> call, Throwable t) {
                dialog.dismiss();
                Tools.noConnection(AddAdvertising.this, mainLyout, new NoConnection() {
                    @Override
                    public void passedMethod() {
                        sendData();
                    }
                });
            }
        });

    }

    int arrimagesSize = 0;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ACTION_GET_CONTENT || requestCode == PICK_FROM_CAMERA) {
            if (data != null) {
                Uri lastData = data.getData();

                if ((lastData == null || data == null)) {

                    if (data.getExtras() != null) {
                        lastData = Tools.specialCameraSelector(this, (Bitmap) data.getExtras().get("data"));
                    }
                    data.setData(lastData);
                }

                try {
                    imagesArr[imageIndex] = convertToBase64(data.getData(), imageViews[imageIndex]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }
    }

    private void getCountries() {
        final String lang = Tools.getCurrentLanguage(this);
        Map<String, String> params = new HashMap<>();
        params.put("lang", lang);
        Webservice.getInstance().getApi().countries(params).enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(Call<CountriesModel> call, Response<CountriesModel> response) {
                if (response.body() != null) {
                    if (response.body().status == 200) {
                        countries = response.body().countries;
                        countriesArr = new String[countries.size()];
                        for (int i = 0; i < countries.size(); i++) {
                            if (lang.equals("en")) {
                                countriesArr[i] = countries.get(i).nameEn;
                            } else {
                                countriesArr[i] = countries.get(i).nameAr;
                            }
                            if (i == (countries.size() - 1)) {
                                showSpinner(spCountry, tvCountry, 1, countriesArr);
                            }

                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<CountriesModel> call, Throwable t) {
                Tools.noConnection(AddAdvertising.this, mainLyout, new NoConnection() {
                    @Override
                    public void passedMethod() {
                        sendData();
                    }
                });
            }
        });
    }


    private void showSpinner(Spinner spinner, final AppCompatTextView appCompatTextView, final int type, final String arr[]) {
        /*
         * 0 = transmision  , 1 = country , 2 = model , 3 =category , 4 bid time , 5 =extrior , 6 = interior,7 = cylinders*/

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_hidden_text, arr);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (type == 0) {
                    if (position == 0) {
                        transmision = "automatic";
                    } else {
                        transmision = "manual";
                    }
                } else if (type == 1) {
                    country = countries.get(position).id;

                } else if (type == 2) {
                    model = brandsModel.carBrands.get(modelPostion).carTypes.get(position).typeId;

                } else if (type == 3) {
                    modelPostion = position;
                    category = brandsModel.carBrands.get(position).brandId;
                    typesArr = new String[brandsModel.carBrands.get(position).carTypes.size()];
                    for (int i = 0; i < brandsModel.carBrands.get(position).carTypes.size(); i++) {
                        typesArr[i] = brandsModel.carBrands.get(position).carTypes.get(i).typeName;
                    }
                    showSpinner(spModel, tvModel, 2, typesArr);
                } else if (type == 4) {
                    bidTimeSt = bidTimeArr[position];
                } else if (type == 5) {
                    extriorSt = metaDataModel.data.exteriorColors.get(position).id;
                } else if (type == 6) {
                    interiorSt = metaDataModel.data.interiorColors.get(position).id;
                } else if (type == 7) {
                    cylinders = cylindersArr[position];
                } else if (type == 8) {
                    beddingType = metaDataModel.data.beddingTypes.get(position).id;


                }
                appCompatTextView.setText(arr[position]);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private String[] fillterArray(String[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != null) {
                count++;
            }
        }
        String[] filteredArray = new String[count];
        for (int j = 0; j < arr.length; j++) {
            if (arr[j] != null) {

                for (int i = 0; i < filteredArray.length; i++) {
                    if (filteredArray[i] == null) {
                        filteredArray[i] = arr[j];
                        break;
                    }


                }
            }
        }
        return filteredArray;
    }
}
