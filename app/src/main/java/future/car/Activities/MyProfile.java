package future.car.Activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import future.car.ImageHandle.CompressObject;
import future.car.ImageHandle.FileOperations;
import future.car.ImageHandle.ImageCompression;
import future.car.Model.CountriesModel;
import future.car.Model.LoginModel;
import future.car.R;
import future.car.Utils.SessionManager;
import future.car.Utils.Tools;
import future.car.WebServices.Webservice;
import io.michaelrocks.paranoid.Obfuscate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
@Obfuscate
public class MyProfile extends AppCompatActivity {
    public final static int IMAGE_CODE = 10, ACTION_GET_CONTENT = 1265, PICK_FROM_CAMERA = 1280;
    Spinner spCountries, spGender;
    LinearLayout llCountries, llGender, llPass, mainLyout;
    AppCompatTextView tvCountries, tvGender;
    AppCompatButton btnChange;
    AppCompatEditText etFirstName, etLastName, etEmail;
    ImageView profilePic, search, back;
    String genderArr[], countriesArr[];
    String countryName = "", imageString = "", genderName = "", email = "",
            firstName = "", lastName = "";
    int countryId = -1;
    List<CountriesModel.Country> countries;
    SessionManager sessionManager;
    boolean firstTime = true;
    Dialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        initializeDesign();
        staticArrays();

    }

    private void staticArrays() {
        genderArr = new String[]{getString(R.string.male), getString(R.string.fmale)};
        showSpinner(spGender, tvGender, 0, genderArr);
        setData();
        getCountries();
    }

    private void initializeDesign() {
        sessionManager = new SessionManager(this);
        mainLyout = findViewById(R.id.ll_my_profile);
        profilePic = findViewById(R.id.image_edit_profile);
        tvCountries = findViewById(R.id.tv_edit_profile_country);
        tvGender = findViewById(R.id.tv_edit_profile_gender);
        llCountries = findViewById(R.id.ll_edit_profile_country);
        llGender = findViewById(R.id.ll_edit_profile_gender);
        spCountries = findViewById(R.id.sp_edit_profile_country);
        spGender = findViewById(R.id.sp_edit_profile_gender);
        llPass = findViewById(R.id.ll_edit_profile_password);
        etEmail = findViewById(R.id.et_my_profile_email);
        etFirstName = findViewById(R.id.et_my_profile_first_name);
        etLastName = findViewById(R.id.et_my_profile_last_name);
        btnChange = findViewById(R.id.btn_edit_profile);
        search = findViewById(R.id.image_myprofile_search);
        back = findViewById(R.id.image_myprofile_back);
        if (Tools.getCurrentLanguage(this).equals("ar")) {
            back.setRotation(180);
        }
        clickLusteners();
    }

    private void clickLusteners() {
        llPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MyProfile.this, ChangePassword.class);
                startActivity(i);
            }
        });

        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
                return;
            }
        });

        btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                change();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Tools.startActivity(MyProfile.this, Search.class);
            }
        });
    }

    public void selectImage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose Image Source");
        builder.setItems(new CharSequence[]{"Gallery", "Camera"},
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Intent galleryIntent = new Intent();
                                galleryIntent.setType("image/*");
                                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                                startActivityForResult(galleryIntent, ACTION_GET_CONTENT);
                                break;
                            case 1:
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent,
                                        PICK_FROM_CAMERA);
                                break;

                            default:
                                break;
                        }
                    }
                });

        builder.show();
    }

    private void showSpinner(Spinner spinner, final AppCompatTextView appCompatTextView, final int type, final String arr[]) {
        /*
         * 0 = gender  , 1 = country */

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_hidden_text, arr);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!firstTime) {

                    if (type == 0) {
                        genderName = arr[position];
                    } else if (type == 1) {
                        countryName = arr[position];
                        countryId = countries.get(position).id;
                    }
                }

                appCompatTextView.setText(arr[position]);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public void change() {
        intialize();
        if (!validate()) {
            Toast.makeText(this, getResources().getString(R.string.change_failed), Toast.LENGTH_SHORT).show();
        } else {
            editProfile();
        }
    }

    public boolean validate() {
        boolean vaild = true;
        if (email.isEmpty() || !Tools.isValidEmail(email)) {
            etEmail.setError(getResources().getString(R.string.Invalid_email));
            vaild = false;


        }

        if (firstName.isEmpty()) {
            etFirstName.setError(getResources().getString(R.string.Invalid_name));
            vaild = false;
        }
        if (lastName.isEmpty()) {
            etLastName.setError(getResources().getString(R.string.Invalid_name));
            vaild = false;
        }


        if (countryId == -1) {
            tvCountries.setText(getString(R.string.choose_country));
            tvCountries.setTextColor(getResources().getColor(R.color.red));
            vaild = false;
        }


        if (genderName.isEmpty()) {
            tvGender.setText(getString(R.string.choose_gender));
            tvGender.setTextColor(getResources().getColor(R.color.red));
            vaild = false;
        }

        return vaild;
    }

    private void setData() {
        Tools.loadImage(profilePic, sessionManager.getUserDetails().get(SessionManager.IMAGE));
        Log.d("setData:", "" + sessionManager.getUserDetails().get(SessionManager.IMAGE));
        if (firstTime) {
            genderName = sessionManager.getUserDetails().get(SessionManager.GENDER);
            //countryId = Integer.parseInt(sessionManager.getUserDetails().get(SessionManager.COUNTRY_ID));
            tvGender.setText("" + genderName);
            tvCountries.setText("" + sessionManager.getUserDetails().get(SessionManager.COUNTRY_NAME));
            etEmail.setText("" + sessionManager.getUserDetails().get(SessionManager.EMAIL));
            String nameArr[] = sessionManager.getUserDetails().get(SessionManager.NAME).split(" ");
            etFirstName.setText("" + nameArr[0]);
            etLastName.setText("" + nameArr[1]);
            firstTime = false;
        }
    }

    public void intialize() {
        email = etEmail.getText().toString().trim();
        firstName = etFirstName.getText().toString().trim();
        lastName = etLastName.getText().toString().trim();

    }

    private void getCountries() {
        final String lang = Tools.getCurrentLanguage(this);
        Map<String, String> params = new HashMap<>();
        params.put("lang", lang);
        Webservice.getInstance().getApi().countries(params).enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(Call<CountriesModel> call, Response<CountriesModel> response) {
                if (response.body() != null) {
                    if (response.body().status == 200) {
                        countries = response.body().countries;
                        countriesArr = new String[countries.size()];
                        for (int i = 0; i < countries.size(); i++) {
                            if (lang.equals("en")) {
                                countriesArr[i] = countries.get(i).nameEn;
                            } else {
                                countriesArr[i] = countries.get(i).nameAr;
                            }
                            if (i == (countries.size() - 1)) {
                                showSpinner(spCountries, tvCountries, 1, countriesArr);
                            }
                        }


                    }
                }
            }

            @Override
            public void onFailure(Call<CountriesModel> call, Throwable t) {
                Tools.noConnection(MyProfile.this, mainLyout, new future.car.Utils.NoConnection() {
                    @Override
                    public void passedMethod() {
                        getCountries();
                    }
                });
            }
        });

    }

    private void editProfile() {
        dialog = Tools.createLoadingBar(this);
        dialog.show();
        LoginModel loginModel = new LoginModel();
        loginModel.first_name = firstName;
        loginModel.last_name = lastName;
        loginModel.country_id = String.valueOf(countryId);
        loginModel.email = email;
        loginModel.gender = genderName;
        if (!imageString.equals("")) {
            loginModel.image = imageString;
        }


        String auth = "Bearer " + sessionManager.getToken();
        Map<String, String> params = new HashMap<>();
        params.put("Content-Type", "application/json");
        params.put("Authorization", auth);
        params.put("Accept", "application/json");

        Webservice.getInstance().getApi().editProfile(params, loginModel).enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                if (response.body() != null) {
                    if (response.body().status == 200) {
                        Log.d("onResponse: ", "" + response.body().user.id);
                        sessionManager.createLoginSession(
                                response.body().user.id,
                                firstName + " " + lastName,
                                email,
                                genderName,
                                sessionManager.getUserDetails().get(SessionManager.TYPE),
                                sessionManager.getUserDetails().get(SessionManager.TOKEN),
                                countryName,
                                String.valueOf(countryId), response.body().user.image);
                        Toast.makeText(MyProfile.this, "" + getResources().getString(R.string.changed_successfully), Toast.LENGTH_SHORT).show();

                        finish();

                    } else {
                        Toast.makeText(MyProfile.this, "" + getString(R.string.registration_failed), Toast.LENGTH_SHORT).show();
                    }
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                dialog.dismiss();
                Tools.noConnection(MyProfile.this, mainLyout, new future.car.Utils.NoConnection() {
                    @Override
                    public void passedMethod() {
                        editProfile();
                    }
                });
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACTION_GET_CONTENT || requestCode == PICK_FROM_CAMERA) {
            if (data != null) {
                Uri lastData = data.getData();

                if ((lastData == null || data == null)) {

                    if (data.getExtras() != null) {
                        lastData = Tools.specialCameraSelector(MyProfile.this, (Bitmap) data.getExtras().get("data"));
                    }
                    data.setData(lastData);
                }
                try {
                    FileOperations fileOperations = new FileOperations();
                    String imagePath = fileOperations.getPath(MyProfile.this, data.getData());
                    File file = new File(imagePath);
                    CompressObject compressObject = new ImageCompression(MyProfile.this).compressImage(imagePath, file.getName());
                    profilePic.setImageBitmap(compressObject.getImage());
                    profilePic.setBackground(getResources().getDrawable(R.color.transparent));
                    imageString = fileOperations.BitMapToString(compressObject.getImage());
                    Log.d("editProfile: ", imageString);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
