package future.car.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.HashMap;
import java.util.Map;

import future.car.Adapters.CarRentAdapter;
import future.car.Model.HomeModel;
import future.car.Model.RentCarModel;
import future.car.R;
import future.car.Utils.RecyclerViewClickListener;
import future.car.Utils.Tools;
import future.car.WebServices.Webservice;
import io.michaelrocks.paranoid.Obfuscate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
@Obfuscate
public class RentCar extends AppCompatActivity {
    ImageView back, Search;
    RecyclerView rcvCars;
    LinearLayout mainLayout;
    RentCarModel rentCarModel;
    CarRentAdapter carRentAdapter;
    HomeModel homeModel;
    Dialog dialog ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rent_car);
        initializeDesign();
        getData();
    }

    private void initializeDesign() {
        back =findViewById(R.id.image_rent_back);
        Search =findViewById(R.id.image_rent_search);
        rcvCars =findViewById(R.id.rcv_rent);
        mainLayout =findViewById(R.id.ll_rent_car);
        rcvCars.setLayoutManager(new LinearLayoutManager(this));
        clickLusteners();
        if (Tools.getCurrentLanguage(this).equals("ar")) {
            back.setRotation(180);
        }
    }


    private void clickLusteners() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(RentCar.this,Search.class);
                startActivity(i);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void getData() {
        dialog = Tools.createLoadingBar(this);
        dialog.show();
        final String lang = Tools.getCurrentLanguage(this);
        Map<String, String> params = new HashMap<>();
        params.put("Content-Language", lang);


        Webservice.getInstance().getApi().getRentCars(params).enqueue(new Callback<HomeModel>() {
            @Override
            public void onResponse(Call<HomeModel> call, Response<HomeModel> response) {
                if (response.body() != null) {
                    if (response.body().status == 200) {
                        homeModel = response.body();
                        carRentAdapter = new CarRentAdapter(homeModel.carRent, RentCar.this, new RecyclerViewClickListener() {
                            @Override
                            public void recyclerViewListClicked(View v, int position) {
                                Intent i = new Intent(RentCar.this, Car.class);
                                i.putExtra("carDetails", homeModel.carRent.get(position));
                                i.putExtra("selected",1);
                                startActivity(i);

                            }
                        });
                        rcvCars.setAdapter(carRentAdapter);
                    }

                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<HomeModel> call, Throwable t) {
                dialog.dismiss();
                Tools.noConnection(RentCar.this, mainLayout, new future.car.Utils.NoConnection() {
                    @Override
                    public void passedMethod() {
                        getData();
                    }
                });

            }
        });
    }


}