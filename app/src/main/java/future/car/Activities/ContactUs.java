package future.car.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import future.car.Model.ContactUsModel;
import future.car.Model.MainResponse;
import future.car.Model.MetaDataModel;
import future.car.R;
import future.car.Utils.NoConnection;
import future.car.Utils.SessionManager;
import future.car.Utils.Tools;
import future.car.WebServices.Webservice;
import io.michaelrocks.paranoid.Obfuscate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
@Obfuscate
public class ContactUs extends AppCompatActivity implements View.OnClickListener {
    AppCompatTextView tvEmail1, tvEmail2, tvPhone3, tvPhone1, tvPhone2, tvSend;
    AppCompatEditText etEmail, etTitle, etMessage;
    LinearLayout mainLayout;
    Dialog dialog;
    ImageView ivSearch, ivBack;
    SessionManager sessionManager;
    String stEmail = "", stTitle = "", stMessage = "";
    MetaDataModel metaDataModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        initializeDesign();
    }

    private void initializeDesign() {
        mainLayout = findViewById(R.id.ll_contact_us);
        ivBack = findViewById(R.id.image_contactus_back);
        ivSearch = findViewById(R.id.image_contactus_search);
        etEmail = findViewById(R.id.et_contactus_email);
        etTitle = findViewById(R.id.et_contactus_title);
        etMessage = findViewById(R.id.et_contactus_message);
        tvEmail1 = findViewById(R.id.tv_contactus_email1);
        tvEmail2 = findViewById(R.id.tv_contactus_email2);
        tvPhone1 = findViewById(R.id.tv_contactus_phone1);
        tvPhone2 = findViewById(R.id.tv_contactus_phone2);
        tvPhone3 = findViewById(R.id.tv_contactus_phone3);
        tvSend = findViewById(R.id.tv_contactus_send);
        sessionManager = new SessionManager(this);
        dialog = Tools.createLoadingBar(this);
        tvEmail1.setOnClickListener(this);
        tvEmail2.setOnClickListener(this);
        tvPhone1.setOnClickListener(this);
        tvPhone2.setOnClickListener(this);
        tvPhone3.setOnClickListener(this);
        if (Tools.getCurrentLanguage(this).equals("ar")) {
            ivBack.setRotation(180);
        }
        getMetaData();
        clickLusteners();
    }


    private void clickLusteners() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tvSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                send();
            }
        });
        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ContactUs.this, Search.class);
                startActivity(i);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void send() {
        getTexts();
        if (!validate()) {
            Toast.makeText(this, getResources().getString(R.string.send_failed), Toast.LENGTH_SHORT).show();
        } else {
            contactUs();
        }
    }

    private void contactUs() {
        ContactUsModel contactUsModel = new ContactUsModel();
        String auth = "Bearer " + sessionManager.getToken();
        Map<String, String> params = new HashMap<>();
        params.put("Content-Type", "application/json");
        params.put("Authorization", auth);
        params.put("Accept", "application/json");

        Webservice.getInstance().getApi().contactUs(params, contactUsModel).enqueue(new Callback<MainResponse>() {
            @Override
            public void onResponse(Call<MainResponse> call, Response<MainResponse> response) {
                if (response.body() != null) {
                    if (response.body().status == 200) {
                        Toast.makeText(ContactUs.this, "" + getResources().getString(R.string.message_sent), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ContactUs.this, "" + getResources().getString(R.string.send_failed), Toast.LENGTH_SHORT).show();

                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<MainResponse> call, Throwable t) {
                dialog.dismiss();
                Tools.noConnection(ContactUs.this, mainLayout, new NoConnection() {
                    @Override
                    public void passedMethod() {
                        contactUs();
                    }
                });
            }
        });
    }


    public boolean validate() {
        boolean vaild = true;
        if (stEmail.isEmpty() || !Tools.isValidEmail(stEmail)) {
            etEmail.setError(getResources().getString(R.string.Invalid_email));
            vaild = false;
        }

        if (stTitle.isEmpty()) {
            etTitle.setError(getResources().getString(R.string.Invalid_name));
            vaild = false;
        }
        if (stMessage.isEmpty()) {
            etMessage.setError(getResources().getString(R.string.Invalid_name));
            vaild = false;
        }

        return vaild;
    }

    public void getTexts() {
        stEmail = etEmail.getText().toString().trim();
        stMessage = etMessage.getText().toString().trim();
        stTitle = etTitle.getText().toString().trim();

    }


    private void getMetaData() {
        Type type = new TypeToken<MetaDataModel>() {
        }.getType();
        metaDataModel = (MetaDataModel) Tools.getObjList(this, "MetaDataModel", type);
        tvEmail1.setText(metaDataModel.data.emails.email1);
        tvEmail2.setText(metaDataModel.data.emails.email2);
        tvPhone1.setText(metaDataModel.data.mobiles.mobile1);
        tvPhone2.setText(metaDataModel.data.mobiles.mobile2);
        tvPhone3.setText(metaDataModel.data.mobiles.mobile3);
    }

    @Override
    public void onClick(View v) {
        if (v == tvEmail1) {
            sendEmail(tvEmail1.getText().toString());
        } else if (v == tvEmail2) {
            sendEmail(tvEmail2.getText().toString());
        } else if (v == tvPhone1) {
            Tools.makeCall(ContactUs.this, tvPhone1.getText().toString());
        } else if (v == tvPhone2) {
            Tools.makeCall(ContactUs.this, tvPhone2.getText().toString());
        } else if (v == tvPhone3) {
            Tools.makeCall(ContactUs.this, tvPhone3.getText().toString());
        }
    }

    private void sendEmail(String email) {
        try {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", email, null));
            startActivity(Intent.createChooser(emailIntent, "Send email..."));
        } catch (Exception e) {
            e.getStackTrace();
        }
    }


}
