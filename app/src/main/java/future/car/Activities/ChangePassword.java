package future.car.Activities;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import future.car.Model.LoginModel;
import future.car.R;
import future.car.Utils.NoConnection;
import future.car.Utils.SessionManager;
import future.car.Utils.Tools;
import future.car.WebServices.Webservice;
import io.michaelrocks.paranoid.Obfuscate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
@Obfuscate
public class ChangePassword extends AppCompatActivity {
    Dialog dialog;
    ImageView back , search;
    AppCompatEditText etOldPass,etNewPass ,etConfirmPass;
    AppCompatButton change;
    LinearLayout mainLayout;
    String oldPass = "" , newPass="",confirmPass="";
    SessionManager sessionManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        initializeDesign();
    }

    private void initializeDesign() {
        sessionManager = new SessionManager(this);
        dialog = Tools.createLoadingBar(this);
        mainLayout = findViewById(R.id.ll_change_pass);
        etOldPass = findViewById(R.id.et_old_pass);
        etNewPass = findViewById(R.id.et_new_pass);
        etConfirmPass = findViewById(R.id.et_new_pass_confirm);
        back = findViewById(R.id.image_change_pass_back);
        search = findViewById(R.id.image_change_pass_search);
        change = findViewById(R.id.btn_change_pass);
        if (Tools.getCurrentLanguage(this).equals("ar")){
            back.setRotation(180);
        }
        clickLusteners();
    }
    public void change() {
        intialize();
        if (!validate()) {
            Toast.makeText(this, getResources().getString(R.string.change_failed), Toast.LENGTH_SHORT).show();
        } else {
            changePass();
        }
    }

    public boolean validate() {
        boolean vaild = true;


        if (oldPass.isEmpty()) {
            etOldPass.setError(getResources().getString(R.string.invaild_input));
            vaild = false;
        }
        if (newPass.isEmpty()||newPass.length()<8) {
            etNewPass.setError(getResources().getString(R.string.password_check));
            vaild = false;
        }

        if (confirmPass.isEmpty()) {
            etConfirmPass.setError(getResources().getString(R.string.invaild_input));
            vaild = false;
        }

        if (!newPass.equals(confirmPass)){
            etConfirmPass.setError(getResources().getString(R.string.invaild_confirm_pass));
            vaild = false;
        }

        return vaild;
    }

    public void intialize() {
        oldPass = etOldPass.getText().toString().trim();
        newPass = etNewPass.getText().toString().trim();
        confirmPass = etConfirmPass.getText().toString().trim();

    }

    private void clickLusteners() {
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                change();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Tools.startActivity(ChangePassword.this,Search.class);
            }
        });
    }

    private void changePass(){
        dialog.show();
        LoginModel loginModel = new LoginModel();
        loginModel.oldPassword=oldPass;
        loginModel.password=newPass;
        loginModel.passwordConfirmation=confirmPass;

        String auth = "Bearer " + sessionManager.getToken();
        Map<String, String> params = new HashMap<>();
        params.put("Content-Type", "application/json");
        params.put("Authorization", auth);
        params.put("Accept", "application/json");

        Webservice.getInstance().getApi().changePass(params,loginModel).enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                if (response.body()!=null){
                    if (response.body().status==200){
                        Toast.makeText(ChangePassword.this, ""+getResources().getString(R.string.changed_successfully), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                dialog.dismiss();
                Tools.noConnection(ChangePassword.this, mainLayout, new NoConnection() {
                    @Override
                    public void passedMethod() {
                        changePass();
                    }
                });

            }
        });
    }
}
