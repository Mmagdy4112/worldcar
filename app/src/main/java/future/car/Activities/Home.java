package future.car.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import future.car.Adapters.AdsAdapter;
import future.car.Adapters.CarAuctionHomeAdapter;
import future.car.Adapters.CarBrandsAdapter;
import future.car.Adapters.CarRentHomeAdapter;
import future.car.Adapters.CarSaleHomeAdapter;
import future.car.Adapters.MenuAdapeter;
import future.car.Model.BrandsModel;
import future.car.Model.HomeModel;
import future.car.Model.MenuModel;
import future.car.Model.MetaDataModel;
import future.car.R;
import future.car.Utils.RecyclerViewClickListener;
import future.car.Utils.SessionManager;
import future.car.Utils.Tools;
import future.car.WebServices.Webservice;
import io.michaelrocks.paranoid.Obfuscate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Obfuscate
public class Home extends AppCompatActivity implements AdapterView.OnItemClickListener,
        BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener, View.OnClickListener {
    public static final Integer[] images = {
            R.mipmap.ic_home,
            R.mipmap.ic_buy_car,
            R.mipmap.ic_rent_car,
            R.mipmap.ic_auctions,
            R.mipmap.ic_add_ad,
            R.mipmap.ic_contact_us,
            R.mipmap.ic_share,
            R.mipmap.ic_rate
    };
    public static final Integer[] imagesLogin = {
            R.mipmap.ic_home,
            R.mipmap.ic_buy_car,
            R.mipmap.ic_rent_car,
            R.mipmap.ic_auctions,
            R.mipmap.ic_add_ad,
            R.mipmap.ic_my_ads,
            R.mipmap.ic_profile,
            R.mipmap.ic_contact_us,
            R.mipmap.ic_wallet,
            R.mipmap.ic_share,
            R.mipmap.ic_rate,
            R.mipmap.ic_logout

    };
    public String[] descriptions, descriptionsLogin;
    DrawerLayout drawerLayout;
    ListView menuListView;
    ImageView drawerImage, search, profilePic;
    RecyclerView rcvCarAuc, rcvCarSale, rcvCarRent, rcvCategories;
    ImageView ivAds, ivAds1, ivAds2, ivAds3;
    TextView tvCarAucMore, tvCarSaleMore, tvCarRentMore, tvCategoriesMore, tvUserNameHeader, tvUserEmailHeader;
    HomeModel homeModels;
    CarSaleHomeAdapter carSaleHomeAdapter;
    CarRentHomeAdapter carRentHomeAdapter;
    CarAuctionHomeAdapter carAuctionHomeAdapter;
    LinearLayout mainLayout;
    ViewGroup header;
    List<MenuModel> menuItems;
    CarBrandsAdapter carBrandsAdapter;
    SessionManager session;
    BrandsModel brandsModel;
    Dialog dialogPopUp;
    //    DefaultSliderView defaultSliderView;
    RecyclerView rcvAds;
    AdsAdapter adsAdapter;
    Dialog dialogLoading;

    public static List<BrandsModel.CarBrands> getBrandsList(Context context) {
        String list = Tools.getShared(context, "brands");

        return new Gson().fromJson(list, new TypeToken<List<BrandsModel.CarBrands>>() {
        }.getType());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initializeDesign();
        getMetaData();
        getBrands();
        getHomeData();
    }

    private void ImageLoaderInitialize() {
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration
                .createDefault(getBaseContext()));
    }

    private void initializeDesign() {
        dialogPopUp = Tools.loginPopUp(this);
        dialogLoading = Tools.createLoadingBar(this);
        ImageLoaderInitialize();
        ivAds = findViewById(R.id.iv_home_ads);
        ivAds1 = findViewById(R.id.iv_home_sale);
        ivAds2 = findViewById(R.id.iv_home_cats);
        ivAds3 = findViewById(R.id.iv_home_auction);
        mainLayout = findViewById(R.id.home);
        rcvCarAuc = findViewById(R.id.rcv_home_auction);
        rcvCarSale = findViewById(R.id.rcv_home_sale);
        rcvCarRent = findViewById(R.id.rcv_home_rent);
        rcvCategories = findViewById(R.id.rcv_home_categories);
        tvCarAucMore = findViewById(R.id.tv_home_auction_more);
        tvCarSaleMore = findViewById(R.id.tv_home_sale_more);
        tvCarRentMore = findViewById(R.id.tv_home_rent_more);
        tvCategoriesMore = findViewById(R.id.tv_home_categories_more);
        search = findViewById(R.id.image_home_search);
        drawerImage = findViewById(R.id.image_drawer);
        drawerLayout = findViewById(R.id.drawer_layout);
        menuListView = findViewById(R.id.left_drawer);
        rcvAds = findViewById(R.id.rcv_home_Ads);


        clickLusteners();
        menu();
    }


    private void clickLusteners() {
        ivAds.setOnClickListener(this);
        ivAds1.setOnClickListener(this);
        ivAds2.setOnClickListener(this);
        ivAds3.setOnClickListener(this);
        tvCarSaleMore.setOnClickListener(this);
        tvCarAucMore.setOnClickListener(this);
        tvCarRentMore.setOnClickListener(this);
        search.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent i = null;
        if (v == ivAds) {
            i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(homeModels.paidAdvertisements.get(0).link));
        } else if (v == ivAds1) {
            i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(homeModels.paidAdvertisements.get(1).link));
        } else if (v == ivAds2) {
            i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(homeModels.paidAdvertisements.get(2).link));
        } else if (v == ivAds3) {
            i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(homeModels.paidAdvertisements.get(3).link));
        } else if (v == tvCarSaleMore) {
            i = new Intent(Home.this, Sale.class);
        } else if (v == tvCarAucMore) {
            i = new Intent(Home.this, Auction.class);

        } else if (v == tvCarRentMore) {
            i = new Intent(Home.this, RentCar.class);
        } else if (v == search) {
            i = new Intent(Home.this, Search.class);
        }
        startActivity(i);
    }

    private void getHomeData() {
        homeModels = (HomeModel) getIntent().getSerializableExtra("homeModel");
        if (homeModels==null) {
            dialogLoading.show();
            final String lang = Tools.getCurrentLanguage(this);
            Map<String, String> params = new HashMap<>();
            params.put("Content-Language", lang);

            Webservice.getInstance().getApi().home(params).enqueue(new Callback<HomeModel>() {
                @Override
                public void onResponse(Call<HomeModel> call, Response<HomeModel> response) {
                    if (response.body() != null) {
                        homeModels = response.body();
                        passDataToRecycler();
                    }
                    dialogLoading.dismiss();
                }

                @Override
                public void onFailure(Call<HomeModel> call, Throwable t) {
                    dialogLoading.dismiss();
                    Tools.noConnection(Home.this, mainLayout, new future.car.Utils.NoConnection() {
                        @Override
                        public void passedMethod() {
                            getHomeData();
                        }
                    });

                }
            });
        } else {

            passDataToRecycler();
        }

    }


    private void passDataToRecycler() {
        final Intent i = new Intent(this, Car.class);
        //car sale
        carSaleHomeAdapter = new CarSaleHomeAdapter(homeModels.carBuy, this, new RecyclerViewClickListener() {
            @Override
            public void recyclerViewListClicked(View v, int position) {
                i.putExtra("carDetails", homeModels.carBuy.get(position));
                startActivity(i);

            }
        });
        rcvCarSale.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rcvCarSale.setAdapter(carSaleHomeAdapter);

        //car rent
        carRentHomeAdapter = new CarRentHomeAdapter(homeModels.carRent, this, new RecyclerViewClickListener() {
            @Override
            public void recyclerViewListClicked(View v, int position) {
                i.putExtra("carDetails", homeModels.carRent.get(position));
                startActivity(i);
            }
        });
        rcvCarRent.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rcvCarRent.setAdapter(carRentHomeAdapter);

        //car auction
        carAuctionHomeAdapter = new CarAuctionHomeAdapter(homeModels.carAuction, this, new RecyclerViewClickListener() {
            @Override
            public void recyclerViewListClicked(View v, int position) {
                i.putExtra("carDetails", homeModels.carAuction.get(position));
                startActivity(i);
            }
        });
        rcvCarAuc.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rcvCarAuc.setAdapter(carAuctionHomeAdapter);
        slider();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
//        sliderLayout.stopAutoCycle();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        session = new SessionManager(this);
        if (session.isLoggedIn()) {
            profilePic.setBackground(getResources().getDrawable(R.color.transparent));
            Tools.loadImage(profilePic, session.getUserDetails().get(SessionManager.IMAGE));
        }


    }


    public void menu() {
        descriptions = new String[]{
                getResources().getString(R.string.menu_home),
                getResources().getString(R.string.menu_buy),
                getResources().getString(R.string.menu_rent),
                getResources().getString(R.string.menu_auction),
                getResources().getString(R.string.menu_add_adv),
                getResources().getString(R.string.menu_contact_us),
                getResources().getString(R.string.menu_share),
                getResources().getString(R.string.menu_rate)

        };
        descriptionsLogin = new String[]{
                getResources().getString(R.string.menu_home),
                getResources().getString(R.string.menu_buy),
                getResources().getString(R.string.menu_rent),
                getResources().getString(R.string.menu_auction),
                getResources().getString(R.string.menu_add_adv),
                getResources().getString(R.string.menu_my_adv),
                getResources().getString(R.string.menu_profile),
                getResources().getString(R.string.menu_contact_us),
                getResources().getString(R.string.menu_wallet),
                getResources().getString(R.string.menu_share),
                getResources().getString(R.string.menu_rate),
                getResources().getString(R.string.menu_logout)

        };


        session = new SessionManager(this);
        Log.d("session", "session: " + session.getUserDetails().get(SessionManager.TOKEN));
        menuItems = new ArrayList<>();
        if (!session.isLoggedIn()) {
            for (int i = 0; i < images.length; i++) {
                MenuModel item = new MenuModel(images[i], descriptions[i]);
                menuItems.add(item);
            }
        } else {

            for (int i = 0; i < imagesLogin.length; i++) {
                MenuModel item = new MenuModel(imagesLogin[i], descriptionsLogin[i]);
                menuItems.add(item);
            }
        }


        drawerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.END);
            }
        });

        LayoutInflater inflater = getLayoutInflater();
        header = (ViewGroup) inflater.inflate(R.layout.menu_header, menuListView, false);
        profilePic = header.findViewById(R.id.image_menu_header);
        tvUserNameHeader = header.findViewById(R.id.tv_menu_header_username);
        tvUserEmailHeader = header.findViewById(R.id.tv_menu_header_email);
        if (session.isLoggedIn()) {
            menuListView.addHeaderView(header, null, false);
        }


        if (!session.getEmail().equals("")) {
            tvUserEmailHeader.setText(session.getUserDetails().get(SessionManager.EMAIL));
            tvUserNameHeader.setText(session.getUserDetails().get(SessionManager.NAME));
            profilePic.setBackground(getResources().getDrawable(R.color.transparent));
            Tools.loadImage(profilePic, session.getUserDetails().get(SessionManager.IMAGE));
            if (Tools.getCurrentLanguage(this).equals("ar")) {
                tvUserEmailHeader.setTextDirection(View.TEXT_DIRECTION_RTL);
                tvUserNameHeader.setTextDirection(View.TEXT_DIRECTION_RTL);
            }
        }

        menuListView.setOnItemClickListener(this);
        MenuAdapeter adapter = new MenuAdapeter(this, menuItems);
        menuListView.setAdapter(adapter);
        drawerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        Intent i;
        switch (position) {
            case 0:

                break;
            case 1:
                if (!session.isLoggedIn()) {
                    i = new Intent(Home.this, Sale.class);
                    startActivity(i);
                }
                break;
            case 2:
                if (!session.isLoggedIn()) {
                    i = new Intent(Home.this, RentCar.class);
                    startActivity(i);
                } else {
                    i = new Intent(Home.this, Sale.class);
                    startActivity(i);
                }


                break;
            case 3:
                if (!session.isLoggedIn()) {
                    i = new Intent(Home.this, Auction.class);
                    startActivity(i);
                } else {
                    i = new Intent(Home.this, RentCar.class);
                    startActivity(i);
                }

                break;
            case 4:
                if (!session.isLoggedIn()) {
                    dialogPopUp.show();
                } else {
                    i = new Intent(Home.this, Auction.class);
                    startActivity(i);
                }

                break;

            case 5:
                if (!session.isLoggedIn()) {
                    i = new Intent(Home.this, ContactUs.class);
                    startActivity(i);
                } else {
                    i = new Intent(Home.this, AddAdvertisingChoose.class);
                    i.putExtra("brandsModel", brandsModel);
                    startActivity(i);
                }

                break;
            case 6:
                if (session.isLoggedIn()) {
                    i = new Intent(Home.this, MyAdvertising.class);
                    startActivity(i);
                } else {
                    shareApp();
                }
                break;
            case 7:
                if (session.isLoggedIn()) {
                    i = new Intent(Home.this, MyProfile.class);
                    startActivity(i);
                } else {

                    openStore();
                }
                break;

            case 8:
                if (session.isLoggedIn()) {
                    i = new Intent(Home.this, ContactUs.class);
                    startActivity(i);
                }

                break;
            case 9:
                i = new Intent(Home.this, Wallet.class);
                startActivity(i);
                break;
            case 10:
                if (session.isLoggedIn()) {
                    shareApp();
                }
                break;
            case 11:
                if (session.isLoggedIn()) {
                    openStore();
                }
                break;
            case 12:
                if (session.isLoggedIn()) {
                    i = new Intent(Home.this, Splash.class);
                    session.logoutUser();
                    finish();
                    startActivity(i);
                }
                break;


            default:
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    private void getBrands() {
        final String lang = Tools.getCurrentLanguage(this);
        Map<String, String> params = new HashMap<>();
        params.put("Content-Language", lang);

        Webservice.getInstance().getApi().getBrands(params).enqueue(new Callback<BrandsModel>() {
            @Override
            public void onResponse(Call<BrandsModel> call, Response<BrandsModel> response) {
                if (response.body() != null) {
                    if (response.body().status == 200) {
                        brandsModel = response.body();
                        storeArayList();
                        carBrandsAdapter = new CarBrandsAdapter(brandsModel.carBrands, Home.this, new RecyclerViewClickListener() {
                            @Override
                            public void recyclerViewListClicked(View v, int position) {
                                Intent i = new Intent(Home.this, CategoriesCars.class);
                                i.putExtra("title", brandsModel.carBrands.get(position).brandName);
                                i.putExtra("id", brandsModel.carBrands.get(position).brandId);
                                startActivity(i);

                            }
                        });
                        rcvCategories.setLayoutManager(new GridLayoutManager(Home.this, 2, GridLayoutManager.HORIZONTAL, false));
                        rcvCategories.setAdapter(carBrandsAdapter);
                    }

                }
            }

            @Override
            public void onFailure(Call<BrandsModel> call, Throwable t) {
                Tools.noConnection(Home.this, mainLayout, new future.car.Utils.NoConnection() {
                    @Override
                    public void passedMethod() {
                        getBrands();
                    }
                });

            }
        });
    }

    private void getMetaData() {

        Map<String, String> params = new HashMap<>();
        params.put("Content-Type", "application/json");
        params.put("Content-Language", Tools.getCurrentLanguage(this));

        Webservice.getInstance().getApi().getMetaData(params).enqueue(new Callback<MetaDataModel>() {
            @Override
            public void onResponse(Call<MetaDataModel> call, Response<MetaDataModel> response) {
                if (response.body() != null) {
                    Tools.storeArayList(Home.this, response.body(), "MetaDataModel");

                }
            }

            @Override
            public void onFailure(Call<MetaDataModel> call, Throwable t) {
                getMetaData();
            }
        });
    }

    private void openStore() {
        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private void shareApp() {

        Intent sharingIntent = new Intent(
                android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "قم بتحميل mashi" + "\n" + "الأندرويد: " + "\n" + "https://play.google.com/store/apps/details?id=" +
                getPackageName();
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Share With");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT,
                shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    private void storeArayList() {


        Gson gson = new Gson();
        List<BrandsModel.CarBrands> textList = new ArrayList<BrandsModel.CarBrands>();
        textList.addAll(brandsModel.carBrands);
        String jsonText = gson.toJson(textList);
        Tools.setShared(this, "brands", jsonText);
    }

    private void slider() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);

        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? 2 : 1;
            }
        });
        rcvAds.setLayoutManager(gridLayoutManager);

        if (0 < (homeModels.paidAdvertisements.size())) {
            ivAds.setVisibility(View.VISIBLE);
            Tools.loadImage(ivAds,homeModels.paidAdvertisements.get(0).image);
        }
        if (1 < (homeModels.paidAdvertisements.size())) {
            ivAds1.setVisibility(View.VISIBLE);
            Tools.loadImage(ivAds1,homeModels.paidAdvertisements.get(1).image);
        }
        if (2 < (homeModels.paidAdvertisements.size())) {
            ivAds2.setVisibility(View.VISIBLE);
            Tools.loadImage(ivAds2,homeModels.paidAdvertisements.get(2).image);
        }
        if (3 < (homeModels.paidAdvertisements.size())) {
            ivAds3.setVisibility(View.VISIBLE);
            Tools.loadImage(ivAds3,homeModels.paidAdvertisements.get(3).image);
        }
        adsAdapter = new AdsAdapter(homeModels.advertisements, this, new RecyclerViewClickListener() {
            @Override
            public void recyclerViewListClicked(View v, int position) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(homeModels.advertisements.get(position).link));
                startActivity(i);
            }
        });
        rcvAds.setAdapter(adsAdapter);

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


}
//    private void initializeDesign() {
//        clickLusteners();
//    }
//
//    private void clickLusteners() {
//    }
