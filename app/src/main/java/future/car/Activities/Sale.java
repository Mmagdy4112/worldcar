package future.car.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.HashMap;
import java.util.Map;

import future.car.Adapters.CarSaleAdapter;
import future.car.Model.HomeModel;
import future.car.R;
import future.car.Utils.RecyclerViewClickListener;
import future.car.Utils.Tools;
import future.car.WebServices.Webservice;
import io.michaelrocks.paranoid.Obfuscate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
@Obfuscate
public class Sale extends AppCompatActivity {
    ImageView back, Search;
    RecyclerView rcvCars;
    LinearLayout mainLayout;
    HomeModel homeModel;
    CarSaleAdapter carSaleAdapter;

    Dialog dialog;
//    dialog = Tools.createLoadingBar(this);
//    dialog.show();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale);
        initializeDesign();
        getData();
    }

    private void initializeDesign() {
        back = findViewById(R.id.image_sale_back);
        Search = findViewById(R.id.image_sale_search);
        rcvCars = findViewById(R.id.rcv_sale);
        mainLayout = findViewById(R.id.ll_sale_car);
        rcvCars.setLayoutManager(new LinearLayoutManager(this));
        clickLusteners();
        if (Tools.getCurrentLanguage(this).equals("ar")) {
            back.setRotation(180);
        }
    }


    private void clickLusteners() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Sale.this, Search.class);
                startActivity(i);
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void getData() {
        dialog = Tools.createLoadingBar(this);
        dialog.show();

        final String lang = Tools.getCurrentLanguage(this);
        Map<String, String> params = new HashMap<>();
        params.put("Content-Language", lang);

        Webservice.getInstance().getApi().getBuyCars(params).enqueue(new Callback<HomeModel>() {
            @Override
            public void onResponse(Call<HomeModel> call, Response<HomeModel> response) {
                if (response.body() != null) {
                    if (response.body().status == 200) {
                        homeModel = response.body();
                        carSaleAdapter = new CarSaleAdapter(homeModel.carBuy, Sale.this, new RecyclerViewClickListener() {
                            @Override
                            public void recyclerViewListClicked(View v, int position) {
                                Intent i = new Intent(Sale.this, Car.class);
                                i.putExtra("carDetails", homeModel.carBuy.get(position));
                                i.putExtra("selected", 0);
                                startActivity(i);

                            }
                        });
                        rcvCars.setAdapter(carSaleAdapter);
                    }

                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<HomeModel> call, Throwable t) {
                dialog.dismiss();
                Tools.noConnection(Sale.this, mainLayout, new future.car.Utils.NoConnection() {
                    @Override
                    public void passedMethod() {
                        getData();
                    }
                });

            }
        });
    }
}
