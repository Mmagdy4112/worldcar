package future.car.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import future.car.Adapters.TransferAdapter;
import future.car.Model.MainResponse;
import future.car.Model.MetaDataModel;
import future.car.Model.WalletModel;
import future.car.R;
import future.car.Utils.SessionManager;
import future.car.Utils.Tools;
import future.car.WebServices.Webservice;
import io.michaelrocks.paranoid.Obfuscate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
@Obfuscate
public class Wallet extends AppCompatActivity {
    ImageView back;
    RecyclerView rcvData;
    AppCompatTextView tvSend;
    AppCompatEditText etTransferName, etEmail, etPhone, etDetails;
    TransferAdapter transferAdapter;
    MetaDataModel metaDataModel;
    SessionManager sessionManager;
    String stEmail = "", stName = "", stPhone = "", stDetails = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        initializeDesign();
    }

    private void initializeDesign() {
        sessionManager = new SessionManager(this);
        back = findViewById(R.id.image_wallet_back);
        etTransferName = findViewById(R.id.et_wallet_transfer_name);
        etEmail = findViewById(R.id.et_wallet_email);
        etPhone = findViewById(R.id.et_wallet_phone);
        etDetails = findViewById(R.id.et_wallet_transfer_details);
        tvSend = findViewById(R.id.tv_wallet_send);
        rcvData = findViewById(R.id.rcv_wallet_methods);
        rcvData.setLayoutManager(new LinearLayoutManager(this));
        clickLusteners();
        if (Tools.getCurrentLanguage(this).equals("ar")) {
            back.setRotation(180);
        }


        Type type = new TypeToken<MetaDataModel>() {
        }.getType();
        metaDataModel = (MetaDataModel) Tools.getObjList(this, "MetaDataModel", type);
        setData();
    }

    private void setData() {

        transferAdapter = new TransferAdapter(metaDataModel.data.bankAccounts, this);
        rcvData.setAdapter(transferAdapter);
    }


    private void clickLusteners() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tvSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                send();
            }
        });
    }

    public void send() {
        getTexts();
        if (!validate()) {
            Toast.makeText(this, getResources().getString(R.string.send_failed), Toast.LENGTH_SHORT).show();
        } else {
            sendData();
        }
    }

    private void sendData() {
        WalletModel walletModel = new WalletModel();
        walletModel.name=stName;
        walletModel.email=stEmail;
        walletModel.mobile=stPhone;
        walletModel.details=stDetails;
        String auth = "Bearer " + sessionManager.getToken();
        Map<String, String> params = new HashMap<>();
        params.put("Content-Type", "application/json");
        params.put("Authorization", auth);
        params.put("Accept", "application/json");

        Webservice.getInstance().getApi().sendTransferInfo(params,walletModel).enqueue(new Callback<MainResponse>() {
            @Override
            public void onResponse(Call<MainResponse> call, Response<MainResponse> response) {
                if (response.body()!=null){
                    if (response.body().status==200){
                        Toast.makeText(Wallet.this, "dsadas", Toast.LENGTH_SHORT).show();
//                        Toast.makeText(Wallet.this, ""+getResources().getString(R.string.su), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<MainResponse> call, Throwable t) {

            }
        });
    }


    public boolean validate() {
        boolean vaild = true;
        if (stEmail.isEmpty() || !Tools.isValidEmail(stEmail)) {
            etEmail.setError(getResources().getString(R.string.Invalid_email));
            vaild = false;
        }

        if (stName.isEmpty()) {
            etTransferName.setError(getResources().getString(R.string.Invalid_name));
            vaild = false;
        }
        if (stDetails.isEmpty()) {
            etDetails.setError(getResources().getString(R.string.Invalid_name));
            vaild = false;
        }
        if (stPhone.isEmpty()) {
            etPhone.setError(getResources().getString(R.string.Invalid_name));
            vaild = false;
        }

        return vaild;
    }

    public void getTexts() {
        stEmail = etEmail.getText().toString().trim();
        stName = etTransferName.getText().toString().trim();
        stDetails = etDetails.getText().toString().trim();
        stPhone = etPhone.getText().toString().trim();

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
