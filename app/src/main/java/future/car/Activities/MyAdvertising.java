package future.car.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.HashMap;
import java.util.Map;

import future.car.Adapters.MyAdvAdapter;
import future.car.Model.HomeModel;
import future.car.Model.RentCarModel;
import future.car.R;
import future.car.Utils.RecyclerViewClickListener;
import future.car.Utils.SessionManager;
import future.car.Utils.Tools;
import future.car.WebServices.Webservice;
import io.michaelrocks.paranoid.Obfuscate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
@Obfuscate
public class MyAdvertising extends AppCompatActivity {
    ImageView back, Search;
    RecyclerView rcvCars;
    LinearLayout mainLayout;
    RentCarModel rentCarModel;
    MyAdvAdapter myAdvAdapter;
    HomeModel homeModel;
    Dialog dialog;
    SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_advertising);
        initializeDesign();
        getData();
    }

    private void initializeDesign() {
        sessionManager = new SessionManager(this);
        back = findViewById(R.id.image_my_adv_back);
        Search = findViewById(R.id.image_my_adv_search);
        rcvCars = findViewById(R.id.rcv_my_adv);
        mainLayout = findViewById(R.id.ll_my_adv);
        rcvCars.setLayoutManager(new LinearLayoutManager(this));
        clickLusteners();
        if (Tools.getCurrentLanguage(this).equals("ar")) {
            back.setRotation(180);
        }
    }


    private void clickLusteners() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MyAdvertising.this, Search.class);
                startActivity(i);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void getData() {
        dialog = Tools.createLoadingBar(this);
        dialog.show();
        String auth = "Bearer " + sessionManager.getToken();
        Map<String, String> params = new HashMap<>();
        params.put("Content-Type", "application/json");
        params.put("Authorization", auth);
        params.put("Accept", "application/json");

        Webservice.getInstance().getApi().getUserCars(params).enqueue(new Callback<HomeModel>() {
            @Override
            public void onResponse(Call<HomeModel> call, Response<HomeModel> response) {
                if (response.body() != null) {
                    if (response.body().status == 200) {
                        homeModel = response.body();
                        myAdvAdapter = new MyAdvAdapter(homeModel.cars, MyAdvertising.this, new RecyclerViewClickListener() {
                            @Override
                            public void recyclerViewListClicked(View v, int position) {
                                Intent i = new Intent(MyAdvertising.this, Car.class);
                                i.putExtra("carDetails", homeModel.cars.get(position));
                                startActivity(i);

                            }
                        });
                        rcvCars.setAdapter(myAdvAdapter);
                    }

                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<HomeModel> call, Throwable t) {
                dialog.dismiss();
                Tools.noConnection(MyAdvertising.this, mainLayout, new future.car.Utils.NoConnection() {
                    @Override
                    public void passedMethod() {
                        getData();
                    }
                });

            }
        });
    }


}