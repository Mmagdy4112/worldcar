package future.car.Activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration.Builder;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import future.car.R;
import future.car.Utils.Tools;
import io.michaelrocks.paranoid.Obfuscate;

@Obfuscate
public class ZoomedImage extends AppCompatActivity {
    Bitmap bitmap;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoomed_image);
        Tools.setHeaderFooterColor(ZoomedImage.this);
        progressBar=(ProgressBar)findViewById(R.id.progressBar);

        final SubsamplingScaleImageView imageView = (SubsamplingScaleImageView) findViewById(R.id.imageView);




        DisplayImageOptions options = new DisplayImageOptions.Builder()
                //.showImageForEmptyUri(R.drawable.ic_language)
                //.showImageOnFail(R.drawable.ic_language)
                .cacheInMemory(true).cacheOnDisk(true).build();

        Builder img = new Builder(
                getApplicationContext());
        img.threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.loadImage( getIntent().getStringExtra("image"), options,
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri,
                                                  View view, Bitmap loadedImage) {
                        bitmap = loadedImage;
                        imageView.setImage(ImageSource.bitmap(loadedImage));
                        progressBar.setVisibility(View.GONE);
                    }
                });
    }
}
