package future.car.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import future.car.Model.LoginModel;
import future.car.R;
import future.car.Utils.NoConnection;
import future.car.Utils.SessionManager;
import future.car.Utils.Tools;
import future.car.WebServices.Webservice;
import io.michaelrocks.paranoid.Obfuscate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
@Obfuscate
public class SignIn extends AppCompatActivity {
    AppCompatTextView tvSignUp;
    AppCompatEditText etPassword, etEmail;
    AppCompatButton btnSignIn;
    LinearLayout llMain;
    String email = "", password = "";
    SessionManager sessionManager;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        sessionManager = new SessionManager(this);

        initializeDesign();
    }

    private void initializeDesign() {
        llMain = findViewById(R.id.ll_sign_in);
        tvSignUp = findViewById(R.id.tv_signin_signup);
        etPassword = findViewById(R.id.et_signin_password);
        etEmail = findViewById(R.id.et_signin_email);
        btnSignIn = findViewById(R.id.btn_signin);
        clickListeners();
    }

    private void clickListeners() {
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });
        tvSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Tools.startActivity(SignIn.this, SignUp.class);
            }
        });
    }

    public void register() {
        intialize();
        if (!validate()) {
            Toast.makeText(this, getResources().getString(R.string.registration_failed), Toast.LENGTH_SHORT).show();
        } else {
            signIn();
        }
    }


    public boolean validate() {
        boolean vaild = true;
        if (email.isEmpty() || !Tools.isValidEmail(email)) {
            etEmail.setError(getResources().getString(R.string.Invalid_email));
            vaild = false;


        }
        if (password.isEmpty() || password.length() < 8) {
            etPassword.setError(getResources().getString(R.string.password_check));
            vaild = false;
        }


        return vaild;
    }


    public void intialize() {
        email = etEmail.getText().toString().trim();
        password = etPassword.getText().toString().trim();
    }

    private void signIn() {
        dialog = Tools.createLoadingBar(this);
        dialog.show();

        LoginModel loginModel = new LoginModel();
        loginModel.password = password;
        loginModel.email = email;

        Webservice.getInstance().getApi().login(loginModel).enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                if (response.body() != null) {
                    if (response.body().status == 200) {
                        sessionManager.createLoginSession(
                                response.body().user.userId,
                                response.body().user.name,
                                response.body().user.email,
                                response.body().user.gender,
                                response.body().user.type,
                                response.body().user.token,
                                "", "",response.body().user.image);
                        Intent i = new Intent(SignIn.this, Splash.class);
                        finishAffinity();
                        startActivity(i);

                    } else {

                    }


                }else{
                    Toast.makeText(SignIn.this, "" + getString(R.string.invailidEmailOrPass), Toast.LENGTH_SHORT).show();

                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                dialog.dismiss();
                Tools.noConnection(SignIn.this, llMain, new NoConnection() {
                    @Override
                    public void passedMethod() {
                        signIn();
                    }
                });
            }
        });
    }

    private void as(){
//
//        Webservice.getInstance().getApi().getBrands(null."").enqueue(new Callback<BrandsModel>() {
//            @Override
//            public void onResponse(Call<BrandsModel> call, Response<BrandsModel> response) {
//
//            }
//
//            @Override
//            public void onFailure(Call<BrandsModel> call, Throwable t) {
//
//            }
//        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
