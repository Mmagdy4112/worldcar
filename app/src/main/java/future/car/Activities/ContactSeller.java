package future.car.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.HashMap;
import java.util.Map;

import future.car.Adapters.MyAdvAdapter;
import future.car.Model.HomeModel;
import future.car.R;
import future.car.Utils.RecyclerViewClickListener;
import future.car.Utils.SessionManager;
import future.car.Utils.Tools;
import future.car.WebServices.Webservice;
import io.michaelrocks.paranoid.Obfuscate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
@Obfuscate
public class ContactSeller extends AppCompatActivity {
    LinearLayout llMain;
    AppCompatTextView tvName,tvEmail,tvPhone;
    AppCompatButton btnCall;
    RecyclerView rcvData;
    ImageView back;
    MyAdvAdapter myAdvAdapter;
    HomeModel homeModel;
    Dialog dialog;
    SessionManager sessionManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_seller);
        initializeDesign();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initializeDesign() {
        sessionManager = new SessionManager(this);
        tvName = findViewById(R.id.tv_contact_seller_name);
        tvEmail = findViewById(R.id.tv_contact_seller_email);
        tvPhone = findViewById(R.id.tv_contact_seller_phone);
        btnCall = findViewById(R.id.btn_contact_seller_call);
        back = findViewById(R.id.image_contact_seller_back);
        rcvData = findViewById(R.id.rcv_contact_seller);
        llMain = findViewById(R.id.ll_contact_seller);
        rcvData.setLayoutManager(new LinearLayoutManager(this));
        setTexts();
        clickLusteners();
        if (Tools.getCurrentLanguage(this).equals("ar")){
            back.setRotation(180);
            tvEmail.setTextDirection(View.TEXT_DIRECTION_RTL);
            tvPhone.setTextDirection(View.TEXT_DIRECTION_RTL);
            tvName.setTextDirection(View.TEXT_DIRECTION_RTL);
        }
    }

    private void setTexts(){
        tvEmail.setText(getIntent().getExtras().getString("userEmail",""));
        tvPhone.setText(getIntent().getExtras().getString("userMobile",""));
        tvName.setText(getIntent().getExtras().getString("userName",""));
        getData(getIntent().getExtras().getInt("userId",0));

    }

    private void clickLusteners() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Tools.makeCall(ContactSeller.this,tvPhone.getText().toString());
            }
        });
    }

    private void getData(int id) {
        dialog = Tools.createLoadingBar(this);
        dialog.show();
        Map<String, String> params = new HashMap<>();
        params.put("Content-Type", "application/json");
        params.put("Accept", "application/json");
        params.put("Accept-Language", Tools.getCurrentLanguage(this));

        Webservice.getInstance().getApi().getSellerCars(params,id).enqueue(new Callback<HomeModel>() {
            @Override
            public void onResponse(Call<HomeModel> call, Response<HomeModel> response) {
                if (response.body() != null) {
                    if (response.body().status == 200) {
                        homeModel = response.body();
                        myAdvAdapter = new MyAdvAdapter(homeModel.carSeller, ContactSeller.this, new RecyclerViewClickListener() {
                            @Override
                            public void recyclerViewListClicked(View v, int position) {
                                Intent i = new Intent(ContactSeller.this, Car.class);
                                i.putExtra("carDetails", homeModel.carSeller.get(position));
                                startActivity(i);

                            }
                        });
                        rcvData.setAdapter(myAdvAdapter);
                    }

                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<HomeModel> call, Throwable t) {
                dialog.dismiss();
                Tools.noConnection(ContactSeller.this, llMain, new future.car.Utils.NoConnection() {
                    @Override
                    public void passedMethod() {
                        getData(getIntent().getExtras().getInt("userId",0));
                    }
                });

            }
        });
    }


}
