package future.car.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import future.car.Model.CountriesModel;
import future.car.Model.LoginModel;
import future.car.R;
import future.car.Utils.NoConnection;
import future.car.Utils.SessionManager;
import future.car.Utils.Tools;
import future.car.WebServices.Webservice;
import io.michaelrocks.paranoid.Obfuscate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
@Obfuscate
public class SignUp extends AppCompatActivity {
    LinearLayout llCountry, llType, llGender,llMain;
    AppCompatTextView tvSignIn, tvCountry, tvGender, tvType;
    AppCompatEditText etFirstName, etLastName, etPassword, etEmail,etPhone;
    AppCompatButton btnSignup;
    String countryId = "", countryName = "", genderId = "", genderName = "", phone = "", typeName = "", email = "", password = "",
            firstName = "", lastName = "", imageString = "";
    Spinner spGender, spCountry, spType;
    SessionManager sessionManager;
    String genderArr[], typeArr[], countriesArr[];
    List<CountriesModel.Country> countries;
    Dialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        sessionManager = new SessionManager(this);
        initializeDesign();
        staticArrays();
        getCountries();
    }

    private void staticArrays() {
        genderArr = new String[]{getString(R.string.male), getString(R.string.fmale)};
        typeArr = new String[]{getString(R.string.individual_seller), getString(R.string.shop)};
        showSpinner(spGender, 0, genderArr);
        showSpinner(spType, 2, typeArr);
    }

    private void initializeDesign() {
        llMain = findViewById(R.id.ll_sign_up);
        tvSignIn = findViewById(R.id.tv_signup_signin);
        tvCountry = findViewById(R.id.tv_signup_country);
        llCountry = findViewById(R.id.ll_signup_country);
        llGender = findViewById(R.id.ll_signup_gender);
        llType = findViewById(R.id.ll_signup_type);
        tvGender = findViewById(R.id.tv_signup_gender);
        tvType = findViewById(R.id.tv_signup_type);
        etPhone = findViewById(R.id.et_signup_phone);
        etFirstName = findViewById(R.id.et_signup_firstname);
        etLastName = findViewById(R.id.et_signup_lastname);
        etPassword = findViewById(R.id.et_signup_password);
        etEmail = findViewById(R.id.et_signup_email);
        btnSignup = findViewById(R.id.btn_signup);
        spCountry = findViewById(R.id.sp_signup_country);
        spGender = findViewById(R.id.sp_signup_gender);
        spType = findViewById(R.id.sp_signup_type);
        clickListeners();
    }

    private void clickListeners() {
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });
        tvSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Tools.startActivity(SignUp.this, SignIn.class);
            }
        });


    }

    public void register() {
        intialize();
        if (!validate()) {
            Toast.makeText(this, getResources().getString(R.string.registration_failed), Toast.LENGTH_SHORT).show();
        } else {
            signUp();
        }
    }


    public boolean validate() {
        boolean vaild = true;
        if (email.isEmpty() || !Tools.isValidEmail(email)) {
            etEmail.setError(getResources().getString(R.string.Invalid_email));
            vaild = false;


        }
        if (password.isEmpty() || password.length() < 8) {
            etPassword.setError(getResources().getString(R.string.password_check));
            vaild = false;
        }

        if (firstName.isEmpty()) {
            etFirstName.setError(getResources().getString(R.string.Invalid_name));
            vaild = false;
        }
        if (lastName.isEmpty()) {
            etLastName.setError(getResources().getString(R.string.Invalid_name));
            vaild = false;
        }
        if (phone.isEmpty()) {
            etPhone.setError(getResources().getString(R.string.Invalid_phone));
            vaild = false;
        }


        if (countryId.isEmpty()) {
            tvCountry.setText(getString(R.string.choose_country));
            tvCountry.setTextColor(getResources().getColor(R.color.red));
            vaild = false;
        }


        if (typeName.isEmpty()) {
            tvType.setText(getString(R.string.choose_country));
            tvType.setTextColor(getResources().getColor(R.color.red));
            vaild = false;
        }


        if (genderName.isEmpty()) {
            tvGender.setText(getString(R.string.choose_country));
            tvGender.setTextColor(getResources().getColor(R.color.red));
            vaild = false;
        }

        return vaild;
    }

    private void showSpinner(Spinner spinner, final int type, final String arr[]) {
        /*
        * 0 = gender  , 1 = country , 2 = type*/

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_hidden_text, arr);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (arr != null) {
                    if (type == 0) {
                        tvGender.setText(arr[position]);
                        if (position == 0) {
                            genderName = "male";
                        } else {
                            genderName = "fmale";
                        }

                    } else if (type == 1) {
                        tvCountry.setText(arr[position]);
                        countryId = String.valueOf(countries.get(position).id);
                    } else if (type == 2) {
                        tvType.setText(arr[position]);
                        if (position == 0) {
                            typeName = "personal";
                        } else {
                            typeName = "shop";
                        }


                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void intialize() {
        email = etEmail.getText().toString().trim();
        password = etPassword.getText().toString().trim();
        firstName = etFirstName.getText().toString().trim();
        lastName = etLastName.getText().toString().trim();
        phone = etPhone.getText().toString().trim();

    }

    private void signUp() {
        dialog = Tools.createLoadingBar(this);
        dialog.show();
        LoginModel loginModel = new LoginModel();
        loginModel.first_name = firstName;
        loginModel.last_name = lastName;
        loginModel.country_id = countryId;
        loginModel.gender = genderName;
        loginModel.type = typeName;
        loginModel.email = email;
        loginModel.mobile = phone;
        loginModel.password = password;
        loginModel.image = imageString;

        Webservice.getInstance().getApi().register(loginModel).enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                dialog.dismiss();
                if (response.body() != null) {
                    if (response.body().status == 200) {
//                        sessionManager.createLoginSession("",
//                                response.body().user.name,
//                                response.body().user.email,
//                                response.body().user.gender,
//                                response.body().user.type,
//                                response.body().user.token,
//                                countryName,
//                                countryId,"");
//                        Intent i = new Intent(SignUp.this, Splash.class);
//                        finishAffinity();
//                        startActivity(i);

                        Toast.makeText(SignUp.this, ""+getResources().getString(R.string.verify_acc), Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(SignUp.this, SignIn.class);
                        finish();
                        startActivity(i);
                    } else {
                        Toast.makeText(SignUp.this, "" + getString(R.string.registration_failed), Toast.LENGTH_SHORT).show();
                    }
                }else{

                    Toast.makeText(SignUp.this, "" + getString(R.string.already_registered_username), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                dialog.dismiss();
                Tools.noConnection(SignUp.this, llMain, new NoConnection() {
                    @Override
                    public void passedMethod() {
                        signUp();
                    }
                });

            }
        });

    }

    private void getCountries() {
        final String lang = Tools.getCurrentLanguage(this);
        Map<String, String> params = new HashMap<>();
        params.put("lang", lang);
        Webservice.getInstance().getApi().countries(params).enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(Call<CountriesModel> call, Response<CountriesModel> response) {
                if (response.body() != null) {
                    if (response.body().status == 200) {
                        countries = response.body().countries;
                        countriesArr = new String[countries.size()];
                        for (int i = 0; i < countries.size(); i++) {
                            if (lang.equals("en")) {
                                countriesArr[i] = countries.get(i).nameEn;
                            } else {
                                countriesArr[i] = countries.get(i).nameAr;
                            }
                            if (i == (countries.size() - 1)) {
                                showSpinner(spCountry, 1, countriesArr);
                            }

                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<CountriesModel> call, Throwable t) {

            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
