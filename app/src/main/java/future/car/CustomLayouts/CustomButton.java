package future.car.CustomLayouts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by mac on 5/22/17.
 */
public class CustomButton extends Button {
    public CustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }
    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CustomButton(Context context) {

        super(context);
        init(context);
    }

    private void init(Context context) {
        if (!isInEditMode()) {
            Typeface tf =null;
//			if(Tools.currentLanguage.equals("ar"))
//				tf= Typeface.createFromAsset(getContext().getAssets(),
//						"arabic_font.TTF");
//			else tf= Typeface.createFromAsset(getContext().getAssets(),
//					"font.TTF");
                tf = Typeface.createFromAsset(getContext().getAssets(),
                        "cocon.ttf");

            setTypeface(tf);
            setTransformationMethod(null);
        }
    }

}