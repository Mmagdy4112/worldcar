package future.car.CustomLayouts;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by mac on 5/21/17.
 */

public class Font  extends TextView {
    String languageToLoad ="ar";


    public Font(Context context) {
        super(context);
        setFont(context);
    }

    public Font(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context);
    }

    public Font(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont(context);

    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    public void setFont(Context context){

            Typeface face = Typeface.createFromAsset(context.getAssets(), "cocon.ttf");
            //setTextColor();
            this.setTypeface(face);


    }

}