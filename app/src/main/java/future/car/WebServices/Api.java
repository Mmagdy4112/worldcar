package future.car.WebServices;

import java.util.Map;

import future.car.Model.AddCarModel;
import future.car.Model.BrandsModel;
import future.car.Model.CarBidModel;
import future.car.Model.ContactUsModel;
import future.car.Model.CountriesModel;
import future.car.Model.HomeModel;
import future.car.Model.LoginModel;
import future.car.Model.MainResponse;
import future.car.Model.MetaDataModel;
import future.car.Model.SearchModel;
import future.car.Model.WalletModel;
import io.michaelrocks.paranoid.Obfuscate;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by Mohamed Magdy on 11/02/2017.
 */
@Obfuscate
public interface Api {

    @POST("login")
    Call<LoginModel> login(@Body LoginModel loginModel);

    @POST("register")
    Call<LoginModel> register(@Body LoginModel loginModel);

    @POST("car/add")
    Call<MainResponse> addCar(@HeaderMap Map<String, String> params,@Body AddCarModel addCarModel);

    @GET("countries")
    Call<CountriesModel> countries(@QueryMap Map<String, String> params);

    @GET("home")
    Call<HomeModel> home(@HeaderMap Map<String, String> params);

    @GET("car/buy/all")
    Call<HomeModel> getBuyCars(@HeaderMap Map<String, String> params);

    @GET("car/rent/all")
    Call<HomeModel> getRentCars(@HeaderMap Map<String, String> params);

    @GET("car/auction/all")
    Call<HomeModel> getAuction(@HeaderMap Map<String, String> params);

    @GET("car/brand/all")
    Call<BrandsModel> getBrands(@HeaderMap Map<String, String> params);

    @POST("car/brand")
    Call<HomeModel> getBrandCars(@Body HomeModel.CarBrand homeModel, @HeaderMap Map<String, String> params);

    @GET("user/cars")
    Call<HomeModel> getUserCars(@HeaderMap Map<String, String> params);

    @GET("meta_data")
    Call<MetaDataModel> getMetaData(@HeaderMap Map<String, String> params);

    @GET("seller/ads/{seller_id}")
    Call<HomeModel> getSellerCars(@HeaderMap Map<String, String> params,@Path("seller_id")int id);

    @POST("user/profile/update")
    Call<LoginModel> editProfile(@HeaderMap Map<String, String> param,@Body LoginModel loginModel);

    @POST("car/search")
    Call<HomeModel> seatch(@HeaderMap Map<String, String> param,@Body SearchModel searchModel);

    @POST("user/password/update")
    Call<LoginModel> changePass(@HeaderMap Map<String, String> param,@Body LoginModel loginModel);

    @POST("car/brand")
    Call<MainResponse> contactUs(@HeaderMap Map<String, String> params , @Body ContactUsModel contactUsModel);

    @POST("car/bid/show")
    Call<CarBidModel> showBid(@HeaderMap Map<String, String> params , @Body CarBidModel carBidModel);


    @POST("car/bid/send")
    Call<MainResponse> bidOnCar(@HeaderMap Map<String, String> params , @Body CarBidModel carBidModel);

    @POST("user/bank/transfer")
    Call<MainResponse> sendTransferInfo(@HeaderMap Map<String, String> params, @Body WalletModel walletModel);
}
