package future.car.WebServices;

import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mohamed Magdy on 11/02/2017.
 */

public class Webservice {
    private static Webservice instance;
    private Api api ;
    public Webservice(){

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient.Builder().
                connectTimeout(5, TimeUnit.MINUTES).
                readTimeout(5, TimeUnit.MINUTES).
                writeTimeout(5, TimeUnit.MINUTES).
                addInterceptor(loggingInterceptor).
                build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(URrls.MAIN_URL)
                .client(okHttpClient)
                .build();
        api = retrofit.create(Api.class);


    }

;

    public static Webservice getInstance(){
        if (instance == null){
            instance = new Webservice();
        }
        return instance;
    }
    public Api getApi(){
        return api;
    }

    public static void startActivity(Context context, Class<?> activity)
    {
        Intent intent = new Intent(context,activity);
        context.startActivity(intent);

    }


}
